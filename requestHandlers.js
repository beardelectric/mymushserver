/**
 * Created by VenoM on 22.02.2017.
 */
var exec = require("child_process").exec;
var iconv = require('iconv-lite');
var mysqlfunctions = require("./mysqlfunctions");
var url = require("url");

function responseTrue(banStatus, banTime, response){
    var conf={}
    var nowDate= new Date();
    var delta = nowDate.getTime() - banTime;
    delta = Math.floor(delta/1000/60);
    delta = 60 - delta;
    conf["banTime"] = delta;
    conf["banStatus"] = banStatus;
    var strJSON = JSON.stringify(conf);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, X-Ijt, Access-Control-Request-Method, Access-Control-Request-Headers");
    response.write(strJSON);
    response.end();
}

/*********************************************************************
 * Function:         checkConnTable(fields, clientHttpConnTable, request, pathname)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:            fields, clientHttpConnTable, request, pathname
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function checkConnTable(fields, clientHttpConnTable, request, pathname, response){
    if (fields.ban == undefined)
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
        response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, X-Ijt, Access-Control-Request-Method, Access-Control-Request-Headers");
        response.end();
    }
    if (fields.ban == true)//check fo data
    {
        if (clientHttpConnTable.length == 0)
        {
            clientHttpConnTable.unshift({//добавить нового клиента в таблицу подключений http
                Login   : fields.login,
                IP      : url.parse(request.headers.origin).hostname,
                Port    : url.parse(request.headers.origin).port,
                BanTime : fields.bantime,
                Ban     : true
            });
            if (response._header == null)
            {
                responseTrue(clientHttpConnTable[0].Ban, fields.bantime, response);
            }
        }
        clientHttpConnTable.forEach (function(index){//if client issue in conn table - update data
            if (index.IP == url.parse(request.headers.origin).hostname)
            {
                if (index.Ban == false)
                {
                    index.Ban = true;
                    index.BanTime = fields.bantime;
                }
                index.Ban = true;
                if (response._header == null)
                {
                    responseTrue(index.Ban, fields.bantime, response);
                }
            }
            else
            {
                clientHttpConnTable.unshift({//добавить нового клиента в таблицу подключений http
                    Login   : fields.login,
                    IP      : url.parse(request.headers.origin).hostname,
                    Port    : url.parse(request.headers.origin).port,
                    BanTime :fields.bantime,
                    Ban     :true
                });
                if (response.header == null)
                {
                    responseTrue(fields.ban, fields.bantime, response);
                }
            }
        })
    }
    if (fields.ban == false)
    {
        if (clientHttpConnTable.length != 0)
        {
            clientHttpConnTable.forEach (function(index){//if client issue in conn table - update data
                if (index.IP == url.parse(request.headers.origin).hostname)
                {
                    var nowDate= new Date();
                    var delta = nowDate.getTime() - index.BanTime;
                    delta = Math.floor(delta/1000/60);
                    if (delta >= 60)
                    {
                        index.Ban = false;
                        responseTrue(index.Ban, index.BanTime, response);
                    }
                    else {
                        index.Ban = true;
                        responseTrue(index.Ban, index.BanTime, response);
                    }
                }
            })
        }
        else{
            responseTrue(fields.ban, fields.BanTime, response);
        }
    }
}

/*********************************************************************
 * Function:         clientLogin(response)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:            Login, password
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function getAccauntInfo(response, fields){
    console.log("Request handler 'getAccauntInfo' was called.");
    mysqlfunctions.selectAcc().then(function(rows){
        if (rows!="undefined" && fields.login != undefined)
        {
            rows.forEach(function (index) {
                if(index.Login == fields.login && index.Password == fields.password && index.Customer_ID != undefined)//validate login fields
                {
                    resHttp(response, index);//send data and headers to cur client
                }
                else//cur user dont registeret in MySQL Acc table
                {
                    var userInfo={}
                    userInfo["Login"] = fields.login;
                    userInfo["Password"] = fields.password;
                    resHttp(response, userInfo);//send data and headers to cur client
                }
            })
        }
        else{
            resHttp(response, "");//send data and headers to cur client
        }
    })
}


/*********************************************************************
 * Function:         availableSensors(response)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function getSensors(response, fields){
    console.log("Request handler 'getSensors' was called.");
    mysqlfunctions.selectSensors(fields).then(function(rows){
        if (rows!="undefined")
        {
            var sensors = new Array;//select all sensors without relays
            for (i=0; i<rows.length; i++)
            {
                if (rows[i].Type != "R")
                {
                    sensors.push(rows[i])
                }
            }
            for(i=0;i<sensors.length;i++)
            {
                var tempDate = new Array();
                tempDate.push(sensors[i].Date.getFullYear());
                tempDate.push(sensors[i].Date.getMonth());
                tempDate[1]++;
                tempDate.push(sensors[i].Date.getDate());
                sensors[i].Date = tempDate.join('-');
            }
            resHttp(response, sensors);//send data and headers to cur client
        }
        else{
            resHttp(response, "");//send data and headers to cur client
            // response.end();
        }
    })
}

/*********************************************************************
 * Function:         setSensorName(response)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function setSensorName(response, fields){
    if (fields.Name != undefined) {
        console.log("Request handler 'setSensorName' was called.");
        mysqlfunctions.setSensorName(fields).then(function (sensors) {
            if (sensors != undefined && fields.Name != undefined) {
                resHttp(response, true);//send data and headers to cur client
                var arr;
                for(h=0;h<index.client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                {
                    arr = index.client_conn_table[h];
                    if (arr.Master_ID_1 == fields.Master_ID || arr.Master_ID_2 == fields.Master_ID || arr.Master_ID_3 == fields.Master_ID || arr.Master_ID_4 == fields.Master_ID || arr.Master_ID_5 == fields.Master_ID)//если есть такой MASTER_ID в клиентской таблице подключений?
                    {
                        index.streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                var data = 'SensorName SET';
                                // var data='V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V;
                                // var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                // data='$'+'V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V+','+calc_checksum+'\r\n';//формировать строку на отправку
                                stream.write(data, 'utf8');
                            }
                        });
                    }
                }
            }
            else {
                resHttp(response, "");//send data and headers to cur client
            }
        })
    }
    else {
        resHttp(response, "");//send data and headers to cur client
    }
}

/*********************************************************************
 * Function:         getTempChartData(response, data)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^
 *
 * Overview:         This function response MYSQL Temp data
 *
 * Note:
 ********************************************************************/
function getTempChartData(response, fields){
    if(fields.LastID != undefined)
    {
        console.log("Request handler 'getTempChartData' was called.");
        mysqlfunctions.selectLastSensLogTemp(fields).then(function(sensors){
            if (sensors!="undefined")
            {
                var tempArray = new Array();
                if (sensors.length > 800000){
                    for(i=300000;i<sensors.length;i++)
                    {
                        tempArray.push(sensors[i]);
                    }
                    sensors = tempArray;
                }

                for(i=0;i<sensors.length;i++)
                {
                    var tempDate = new Array();
                    tempDate.push(sensors[i].Date.getFullYear());
                    tempDate.push(sensors[i].Date.getMonth());
                    tempDate[1]++;
                    tempDate.push(sensors[i].Date.getDate());
                    sensors[i].Date = tempDate;
                    sensors[i].Time = sensors[i].Time.split(':');
                    sensors[i].Time[0] = +sensors[i].Time[0];
                    sensors[i].Time[1] = +sensors[i].Time[1];
                    sensors[i].Time[2] = +sensors[i].Time[2];
                }
                resHttp(response, sensors);//send data and headers to cur client
            }
            else{
                resHttp(response, "");//send data and headers to cur client
            }
        })
    }
    else{
        resHttp(response, "");//send data and headers to cur client
    }

}
/*********************************************************************
 * Function:         getHudmChartData(response, data)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^
 *
 * Overview:         This function response MYSQL Hudm data
 *
 * Note:
 ********************************************************************/
function getHudmChartData(response, fields){
    if(fields.LastID != undefined)
    {
        console.log("Request handler 'getHudmChartData' was called.");
        mysqlfunctions.selectLastSensLogHudm(fields).then(function(sensors){
            if (sensors!="undefined")
            {
                var tempArray = new Array();
                if (sensors.length > 800000){
                    for(i=300000;i<sensors.length;i++)
                    {
                        tempArray.push(sensors[i]);
                    }
                    sensors = tempArray;
                }

                for(i=0;i<sensors.length;i++)
                {
                    var tempDate = new Array();
                    tempDate.push(sensors[i].Date.getFullYear());
                    tempDate.push(sensors[i].Date.getMonth());
                    tempDate[1]++;
                    tempDate.push(sensors[i].Date.getDate());
                    sensors[i].Date = tempDate;
                    sensors[i].Time = sensors[i].Time.split(':');
                    sensors[i].Time[0] = +sensors[i].Time[0];
                    sensors[i].Time[1] = +sensors[i].Time[1];
                    sensors[i].Time[2] = +sensors[i].Time[2];
                }
                resHttp(response, sensors);//send data and headers to cur client
            }
            else{
                resHttp(response, "");//send data and headers to cur client
            }
        })
    }
    else{
        resHttp(response, "");//send data and headers to cur client
    }

}

/*********************************************************************
 * Function:         getSensorsLog(response, data)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^
 *
 * Overview:         This function response MYSQL sensors_log data
 *
 * Note:             inpup: log period, type of sensor
 ********************************************************************/
function getSensorsLog(response, fields){
    if(fields.dateEnd != undefined)
    {
        console.log("Request handler 'getSensorsLog' was called.");
        mysqlfunctions.getSensorsLog(fields).then(function(sensors){
            if (sensors!="undefined")
            {
                resHttp(response, sensors);//send data and headers to cur client
            }
            else{
                resHttp(response, "");//send data and headers to cur client
            }
        })
    }
    else{
        resHttp(response, "");//send data and headers to cur client
    }

}

/*********************************************************************
 * Function:         setLowerValue(response)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function setLowerValue(response, fields){
    if (fields.Lower_Value != undefined) {
        console.log("Request handler 'setLowerValue' was called.");
        mysqlfunctions.setLowerValue(fields).then(function (sensors) {
            if (sensors != undefined && fields.Name != undefined) {
                resHttp(response, true);//send data and headers to cur client
                var arr;
                for(h=0;h<index.client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                {
                    arr = index.client_conn_table[h];
                    if (arr.Master_ID_1 == fields.Master_ID || arr.Master_ID_2 == fields.Master_ID || arr.Master_ID_3 == fields.Master_ID || arr.Master_ID_4 == fields.Master_ID || arr.Master_ID_5 == fields.Master_ID)//если есть такой MASTER_ID в клиентской таблице подключений?
                    {
                        index.streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                var data = 'LowerValue SET';
                                // var data='V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V;
                                // var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                // data='$'+'V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V+','+calc_checksum+'\r\n';//формировать строку на отправку
                                stream.write(data, 'utf8');
                            }
                        });
                    }
                }
            }
            else {
                resHttp(response, "");//send data and headers to cur client
            }
        })
    }
    else {
        resHttp(response, "");//send data and headers to cur client
    }
}

/*********************************************************************
 * Function:         setUpperValue(response)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function setUpperValue(response, fields){
    if (fields.Upper_Value != undefined) {
        console.log("Request handler 'setLowerValue' was called.");
        mysqlfunctions.setUpperValue(fields).then(function (sensors) {
            if (sensors != undefined && fields.Name != undefined) {
                resHttp(response, true);//send data and headers to cur client
                var arr;
                for(h=0;h<index.client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                {
                    arr = index.client_conn_table[h];
                    if (arr.Master_ID_1 == fields.Master_ID || arr.Master_ID_2 == fields.Master_ID || arr.Master_ID_3 == fields.Master_ID || arr.Master_ID_4 == fields.Master_ID || arr.Master_ID_5 == fields.Master_ID)//если есть такой MASTER_ID в клиентской таблице подключений?
                    {
                        index.streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                var data = 'UpperValue SET';
                                // var data='V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V;
                                // var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                // data='$'+'V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V+','+calc_checksum+'\r\n';//формировать строку на отправку
                                stream.write(data, 'utf8');
                            }
                        });
                    }
                }
            }
            else {
                resHttp(response, "");//send data and headers to cur client
            }
        })
    }
    else {
        resHttp(response, "");//send data and headers to cur client
    }
}

/*********************************************************************
 * Function:         setDelta(response)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function setDelta(response, fields){
    if (fields.Upper_Value != undefined) {
        console.log("Request handler 'setLowerValue' was called.");
        mysqlfunctions.setDelta(fields).then(function (sensors) {
            if (sensors != undefined && fields.Name != undefined) {
                resHttp(response, true);//send data and headers to cur client
                index.client_conn_table;
                var arr;
                for(h=0;h<index.client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                {
                    arr = index.client_conn_table[h];
                    if (arr.Master_ID_1 == fields.Master_ID || arr.Master_ID_2 == fields.Master_ID || arr.Master_ID_3 == fields.Master_ID || arr.Master_ID_4 == fields.Master_ID || arr.Master_ID_5 == fields.Master_ID)//если есть такой MASTER_ID в клиентской таблице подключений?
                    {
                        index.streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                var data = 'DELTA SET';
                                // var data='V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V;
                                // var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                // data='$'+'V'+','+fields.Master_ID+','+Slave_ID+','+Event+','+Battery_V+','+calc_checksum+'\r\n';//формировать строку на отправку
                                stream.write(data, 'utf8');
                            }
                        });
                    }
                }
            }
            else {
                resHttp(response, "");//send data and headers to cur client
            }
        })

    }
    else {
        resHttp(response, "");//send data and headers to cur client
    }
}

/*********************************************************************
 * Function:         setRelay(response)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^           data or error
 *
 * Overview:         This function response for http request
 *
 * Note:
 ********************************************************************/
function setRelay(response){
    console.log("Request handler 'setRelay' was called.");
    mysqlfunctions.selectMasters().then(function(rows){
        if (rows!=undefined)
        {
            resHttp(response, rows);//send data and headers to cur client
        }
        else{
            resHttp(response, "");//send data and headers to cur client
            response.end();
        }
    })
}

/*********************************************************************
 * Function:         resHttp(response, data)
 *
 * PreCondition:     tcpserver, hpptserver started
 *
 * Input:
 * Return^
 *
 * Overview:         This function send empty response
 *
 * Note:
 ********************************************************************/
function resHttp(response, data){
    var strJSON = JSON.stringify(data);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, X-Ijt, Access-Control-Request-Method, Access-Control-Request-Headers");
    response.write(strJSON);
    response.end();
}





exports.getHudmChartData = getHudmChartData;
exports.getTempChartData = getTempChartData;
exports.checkConnTable = checkConnTable;
exports.getAccauntInfo = getAccauntInfo;
exports.getSensors = getSensors;
exports.setSensorName = setSensorName;
exports.setLowerValue = setLowerValue;
exports.setUpperValue = setUpperValue;
exports.setDelta = setDelta;
exports.setRelay = setRelay;
exports.getSensorsLog = getSensorsLog;

var index = require("./index");
