/**
 * Created by VenoM on 22.02.2017.
 */

var app1 = require('express')();
var http = require("http");
var url = require("url");
var router = require("./router");
formidable = require('formidable'),
    util = require('util');
var requestHandlers = require("./requestHandlers");



function start(route, handle) {
    var clientHttpConnTable = new Array();
    function onRequest(request, response) {
        var form = new formidable.IncomingForm();
        form.parse(request, function(err, fields, files) {
            console.log(fields);
            var pathname = url.parse(request.url).pathname;
            console.log("Request for " + pathname + " received.");
            if (pathname == "/getBanStatus")
            {
                requestHandlers.checkConnTable(fields, clientHttpConnTable, request, pathname, response);
            }
            else {
                route(pathname, handle, response, fields);
            }
        });
    }
    http.createServer(onRequest).listen(81);
    console.log("Server has started.");
};
exports.start = start;



