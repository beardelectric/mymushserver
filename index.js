/**
 * Created by VenoM on 22.02.2017.
 */

var httpserver = require("./httpserver");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var exec = require("child_process").exec;
var mysqlfunctions = require("./mysqlfunctions");

var handle={}
handle["/"]=requestHandlers.start;
handle["/start"]=requestHandlers.start;
handle["/upload"]=requestHandlers.upload;
//**********************get handles***********************
handle["/getBanStatus"]=requestHandlers.getBanStatus;
handle["/getAccauntInfo"]=requestHandlers.getAccauntInfo;
handle["/getSensors"]=requestHandlers.getSensors;
//**********************set handles***********************
handle["/setSensorName"]=requestHandlers.setSensorName;
handle["/setLowerValue"]=requestHandlers.setLowerValue;
handle["/setUpperValue"]=requestHandlers.setUpperValue;
handle["/setDelta"]=requestHandlers.setDelta;
handle["/setRelay"]=requestHandlers.setRelay;
//**********************set sensors data***********************
handle["/getHudmChartData"]=requestHandlers.getHudmChartData;
handle["/getTempChartData"]=requestHandlers.getTempChartData;
//**********************get sensors log***********************
handle["/getSensorsLog"]=requestHandlers.getSensorsLog;

httpserver.start(router.route, handle);








/************TCP SERVER+USBINTERFACE+MYSQL BLOCK***************************
 * Created by VenoM on 26.10.2016.
 */
var fs = require('fs');
var promise = require('promise');
var vow = require('vow');
var url = require("url");
var http = require("http"); //подключить библиотеку http
var net = require("net");// подключить библиотеку net
var string_decoder = require("string_decoder");// подключить библиотеку string_decoder
var mysql = require('mysql');
var connect = require('connect');
var ws = require('ws');
var EventEmitter = require('events');
require('date-utils');
var usb = require('usb');
assert = require('assert');
util = require('util')
const stream = require('stream');



var intel = require('intel');
intel.setLevel(intel.WARN).addHandler(new intel.handlers.File('./error.log'));

//имя, пароль БД
var connection = mysql.createConnection({
    host     : '127.0.0.1',
    user     : '1111',
    password : '1111',
    database : 'oneoneoneone',
    charset  : 'utf8_general_ci'
});
exports.connection = connection;

var streams_usb=Array();
exports.streams_usb = streams_usb;
var streams = Array();
exports.streams = streams;
var dateNow = new Date();
var year=dateNow.getFullYear();
var month=dateNow.getMonth()+1;
var date=dateNow.getDate();
var time={};
time.hours=dateNow.getHours();
time.minutes=dateNow.getMinutes();
time.seconds=dateNow.getSeconds();
var tcp_server={};
tcp_server.port1=65000; // порт сервера 1 для ESP
tcp_server.port2=81; // порт сервера 2 для клиентов
// tcp_server.ip='192.168.117.163' //IP tcp сервера
// tcp_server.ip='192.168.1.93' //IP tcp сервера
// tcp_server.ip='192.168.137.117' //IP tcp сервера
tcp_server.ip='localhost' //IP tcp сервера


var master_conn_table_usb = new Array();//таблица подключений к серверу
exports.master_conn_table_usb = master_conn_table_usb;
var master_conn_table = new Array();//таблица подключений к серверу
exports.master_conn_table = master_conn_table;
var client_conn_table = new Array();//таблица подключений клиентов (tcp)
exports.client_conn_table = client_conn_table;
var test_arr=new Array ({
    host     :'',
    user     :'',
    password :'',
    database :''
});
test_arr.shift();
test_arr.unshift({
    host     :'',
    user     :'',
    password :'',
    database :''
});;
var retdata;




var master_cdc=usb.findByIds(1240, 10); //поиск нашего USB2.0
if (master_cdc!=undefined)
{
    master_cdc.open();//открываем наш порт
    master_cdc.reset(function (error){
        console.log(error);
    })
}
usb.on('attach', function(device)
{
    if (device.deviceDescriptor.idProduct==10 && device.deviceDescriptor.idVendor==1240)//если нашли - открываем порт
    {
        processUSB(device);

    }
});
usb.on('detach', function(device)
{
    if (device.deviceDescriptor.idProduct==10 && device.deviceDescriptor.idVendor==1240)//если мастер отвалился
    {
        var arr;
        var index=streams_usb.indexOf(device);//найти индекс того, который отключился
        if (index!=0)
        {
            streams_usb.splice(index);//удалить если индекс не ноль
        }
        else
        {
            streams_usb.shift();//если ноль
        }
        for (i=0;i<master_conn_table_usb.length;i++)
        {
            arr=master_conn_table_usb[i];
            if (arr.busNumber==device.busNumber && arr.deviceAddress==device.deviceAddress)//если есть такой адрес в таблице подключений
            {
                if(i!=0)
                {
                    var Master_ID_discon=arr.Master_ID;
                    master_conn_table_usb.splice(i);//удалить из таблицы подключений
                }
                else
                {
                    var Master_ID_discon=arr.Master_ID;
                    master_conn_table_usb.shift();//если нулевой элемент массива - удалить через shift()
                }
            }
        }
        add_to_master_log(Master_ID_discon);//записать событие в лог
        for(i=0;i<client_conn_table.length;i++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
        {
            arr = client_conn_table[i];
            if (arr.Master_ID_1 == Master_ID_discon || arr.Master_ID_2 == Master_ID_discon || arr.Master_ID_3 == Master_ID_discon || arr.Master_ID_4 == Master_ID_discon || arr.Master_ID_5 == Master_ID_discon)//есть такой MASTER_ID есть в клиентской таблице подключений?
            {
                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                        var data='$'+'CSS'+','+Master_ID_discon+','+'OFF';
                        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                        data='$'+'CSS'+','+Master_ID_discon+','+'OFF'+','+calc_checksum+'\r\n';//формировать строку на отправку
                        stream.write(data, 'utf8');
                    }
                });
            }
        }
        console.log('Master_device detached: USB.busNumber: ' + device.busNumber +' USB.deviceAddress '+ device.deviceAddress+ ' Master_ID: '+Master_ID_discon);
    }
});


connection.connect(); //подключиться к БД MYSQL

/*********************************************************************
 * Function:         processUSB(master_cdc)
 *
 * PreCondition:     tcpserver started
 *
 * Input:            USB_CDC data
 * Return^           parsed data
 *
 * Overview:         This function parse input string according Mymush protocol
 *
 * Note:
 ********************************************************************/
function processUSB(master_cdc)
{
    var device=master_cdc;
    device.open();//открываем наш порт
    console.log('Master_device attached: USB.busNumber: ' + device.busNumber +' USB.deviceAddress '+ device.deviceAddress);
    var endpoints = device.interfaces[1].endpoints,// интерфейс 1 - интерфейс данных
        inEndpoint = endpoints[1], // конечная точка 1 - входящие данные
        outEndpoint = endpoints[0];// конечная точка 0 - исходящие данные
    device.interfaces[1].claim();// работаем по интерфейсу данных
    inEndpoint.startPoll(3, 64);// запустить обработчик событий
    inEndpoint.on('data', function (data) {
        console.log(data.toString());
        parse_usb_data(data, device.deviceAddress, device.busNumber);
    });
    inEndpoint.on('error', function (error) {
        console.log(error);
    });
    outEndpoint.transfer('$MIS,7B\r\n', function (err) {//послать запрос на MASTER_ID
        console.log(err);
    });
    outEndpoint.transfer('$MFC,BBAA,48\r\n', function (err) {//послать запрос на ПОИСК
        console.log(err);
    });
    master_conn_table_usb.unshift({//добавить нового клиента в таблицу подключений tcp/IP
        busNumber     :'',
        deviceAddress :'',
        Master_ID :''
    });;
    master_conn_table_usb[0].Master_ID="await";
    master_conn_table_usb[0].busNumber=device.busNumber;
    master_conn_table_usb[0].deviceAddress=device.deviceAddress;
    streams_usb.push(device);      //Добавляем в массив потоков
}


//запускаем tcp для работы с мастерами сервер
const server=net.createServer(function(sock,req)
{
    var i;
    temp_arr = new Array();
    console.log('MASTER CONNECTED: ' + sock.remoteAddress +' '+ sock.remotePort);
    intel.error('EVENT: CLIENT CONNECTED: ' + sock.remoteAddress +' '+ sock.remotePort+"\r\n");
    bla = server.getConnections(function(err, count){
        console.log("CLIENTS ONLINE", ":" + count);
    });
    sock.write('$MIS,7B\r\n'); //послать запрос на MASTER_ID
    master_conn_table.unshift({//добавить нового клиента в таблицу подключений tcp/IP
        IP     :'',
        Port :'',
        Master_ID :''
    });;
    master_conn_table[0].Master_ID="await";
    master_conn_table[0].IP=sock.remoteAddress;
    master_conn_table[0].Port=sock.remotePort;
    streams.push(sock);      //Добавляем в массив потоков
    sock.on('data', function(data)
    {
        var i;
        var arr;
        var parsed_data;
        master_conn_table;
        parsed_data=parse_tcp_data(data, sock.remoteAddress, sock.remotePort);
        // if (parsed_data[0]=="SIM")
        // {
        //     for (i = 0; i < master_conn_table.length; i++) {
        //         arr = master_conn_table[i];
        //         if (arr.Port == sock.remotePort && arr.IP == sock.remoteAddress && arr.Master_ID == "await" && parsed_data != "incorrect data" && parsed_data != "bad checksum")//если есть такой адрес в таблице подключений и он ожидает назначение Customer_ID
        //         {
        //             arr.Master_ID = parsed_data[1];
        //             master_conn_table[i] = arr;
        //         }
        //     }
        // }
        console.log('Master_device data received: ' + sock.remoteAddress +' '+ sock.remotePort + ': ' + data);
        intel.error('Master_device data received: ' + sock.remoteAddress +' '+ sock.remotePort + ': ' + data+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
    });
    sock.on('close', function(data)
    {
        var arr;
        var index=streams.indexOf(this);//найти индекс того, который отключился
        if (index!=0)
        {
            streams.splice(index);//удалить если индекс не ноль
        }
        else
        {
            streams.shift();//если ноль
        }
        for (i=0;i<master_conn_table.length;i++)
        {
            arr=master_conn_table[i];
            if (arr.IP==sock.remoteAddress && arr.Port==sock.remotePort)//если есть такой адрес в таблице подключений
            {
                if(i!=0)
                {
                    var Master_ID_discon=arr.Master_ID;
                    master_conn_table.splice(i);//удалить из таблицы подключений
                }
                else
                {
                    var Master_ID_discon=arr.Master_ID;
                    master_conn_table.shift();//если нулевой элемент массива - удалить через shift()
                }
            }
        }
        add_to_master_log(Master_ID_discon);//записать обытие в лог
        for(i=0;i<client_conn_table.length;i++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
        {
            arr = client_conn_table[i];
            if (arr.Master_ID_1 == Master_ID_discon || arr.Master_ID_2 == Master_ID_discon || arr.Master_ID_3 == Master_ID_discon || arr.Master_ID_4 == Master_ID_discon || arr.Master_ID_5 == Master_ID_discon)//есть такой MASTER_ID есть в клиентской таблице подключений?
            {
                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                        var data='$'+'CSS'+','+Master_ID_discon+','+'OFF';
                        var calc_checksum=calc_xor(data);//вычислять контрольную сумму
                        data='$'+'CSS'+','+Master_ID_discon+','+'OFF'+','+calc_checksum+','+'OD'+','+'OA';//формировать строку на отправку
                        stream.write(data, 'utf8');
                    }
                });
            }
        }
        console.log('Master_device disconnected: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
    sock.on('error', function(data)
    {
        console.log('CLIENT CONNECTION ERROR: ' + sock.remoteAddress +' '+ sock.remotePort);
        intel.error('ERROR: Connection, sock.end ' + tcp_server.ip +':'+ tcp_server.port1 + year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
        sock.end();
    });
}).listen(tcp_server.port1, tcp_server.ip);
console.log('Master Server started on ' + tcp_server.ip +':'+ tcp_server.port1);
intel.error('EVENT: Master Server started on ' + tcp_server.ip +':'+ tcp_server.port1 + ' '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");


/*********************************************************************
 * Function:         server1
 *
 * PreCondition:     tcpserver(master server) started
 *
 * Input:            tcp data
 * Return^
 *
 * Overview:         This process tcp connection on port 65000
 *
 * Note:
 ********************************************************************/
const server1=net.createServer(function(sock,req)
{
    console.log('CLIENT CONNECTED: ' + sock.remoteAddress +' '+ sock.remotePort);
    intel.error('EVENT: CLIENT CONNECTED to MASTER Server: ' + sock.remoteAddress +' '+ sock.remotePort+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
    client_conn_table.unshift({//добавить нового клиента в таблицу подключений tcp/IP
        Customer_ID     :'',
        IP     :'',
        Port :'',
        Master_ID_1 :'',
        Master_ID_2 :'',
        Master_ID_3 :'',
        Master_ID_4 :'',
        Master_ID_5 :''
    });;
    client_conn_table[0].Customer_ID="await";
    client_conn_table[0].IP=sock.remoteAddress;
    client_conn_table[0].Port=sock.remotePort;
    streams.push(sock);      //Добавляем в массив потоков
    sock.on('data', function(data)//событие "пришли данные"
    {
        var i;
        var arr;
        var parsed_data;
        parsed_data=parse_tcp_data(data, sock.remoteAddress, sock.remotePort);

        console.log('DATA ' + sock.remoteAddress +' '+ sock.remotePort + ': ' + data);
        intel.error('EVENT: Data received (MASTER Server): ' + sock.remoteAddress +' '+ sock.remotePort+' Data: '+ data +' Timestamp: '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
    });

    sock.on('close', function(data)//событие "клиент отключился"
    {
        var index=streams.indexOf(this);//найти индекс того, который отключился
        if (index!=0)
        {
            streams.splice(index);//удалить если индекс не ноль
        }
        else
        {
            streams.shift();//если ноль
        }
        for (i=0;i<client_conn_table.length;i++)
        {
            arr=client_conn_table[i];
            if (arr.IP==sock.remoteAddress && arr.Port==sock.remotePort)//если есть такой адрес в таблице подключений
            {
                if(i!=0)
                {
                    client_conn_table.splice(i);//удалить из таблицы подключений
                }
                else
                {
                    client_conn_table.shift();//если нулевой элемент массива - удалить через shift()
                }
            }
        }
        console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });

    sock.on('error', function(data)//событие "ошибка сокета"
    {
        console.log('CLIENT CONNECTION ERROR: ' + sock.remoteAddress +' '+ sock.remotePort);
        intel.error('ERROR: Connection, sock.end ' + tcp_server.ip +':'+ tcp_server.port2 + year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
        sock.end();
    });
// sock.end();
}).listen(tcp_server.port2, tcp_server.ip);
console.log('softClient Server started on ' + tcp_server.ip +':'+ tcp_server.port2);
intel.error('EVENT: softClient Server started on ' + tcp_server.ip +':'+ tcp_server.port2 +' '+year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");


/*********************************************************************
 * Function:         parse_usb_data(data)
 *
 * PreCondition:     tcpserver started
 *
 * Input:            usb data
 * Return^           parsed data
 *
 * Overview:         This function parse input string according Mymush protocol
 *
 * Note:            $V,112233,AABB,3.1,CS;  CS=byte-to-byte XOR
 ********************************************************************/
function parse_usb_data(data, deviceAddress, busNumber)
{
    var arrdata;
    var index;
    var data;
    var i;
    var arr;
    var j;
    var calc_checksum;
    var data=data.toString();
    var input_checksum;
    var tempdata = new Array();
    arrdata=data.split('$');
    for (j=1;j<arrdata.length;j++)
    {
        data=arrdata[j];
        tempdata = data.split(',');//преобразовать в массив
        input_checksum=tempdata[tempdata.length-1];//запомнить контрольную сумму принятой строки
        if(input_checksum[0]=="0" && input_checksum.length=='4')
        {
            input_checksum=input_checksum.substring(1,4);
        }
        tempdata=tempdata.splice(0,tempdata.length-1);//удалить элемент массива (контрольную сумму,OD,0A)
        data=tempdata.toString();//получить строку без '$' и контрольной суммы
        calc_checksum = calc_xor(data);
        calc_checksum = calc_xor(data).toString(16).toUpperCase()+"\r\n";//получить контрольную сумму методом побайтового XOR, преобразовать в hex
        if (calc_checksum==input_checksum)
        {
            format_data(tempdata, deviceAddress, busNumber);
        }
        else
        {
            console.log('bad checksum')
            // return "bad checksum";
        }
        // return calc_checksum;
    }

}

/*********************************************************************
 * Function:         parse_tcp_data(data)
 *
 * PreCondition:     tcpserver started
 *
 * Input:            tcp data
 * Return^           parsed data
 *
 * Overview:         This function parse input string according Mymush protocol
 *
 * Note:            $V,112233,AABB,3.1,CS;  CS=byte-to-byte XOR
 ********************************************************************/
function parse_tcp_data(data, remoteAddress, remotePort)
{
    var arrdata;
    var index;
    var data;
    var i;
    var arr;
    var j;
    var calc_checksum;
    var data=data.toString();
    var input_checksum;
    var tempdata = new Array();
    arrdata=data.split('$');
    for (j=1;j<arrdata.length;j++)
    {
        data=arrdata[j];
        // data=data.substring(1,data.length);//удалить $ из начала строки
        tempdata = data.split(',');//преобразовать в массив
        input_checksum=tempdata[tempdata.length-1];//запомнить контрольную сумму принятой строки
        if(input_checksum[0]=="0" && input_checksum.length=='4')
        {
            input_checksum=input_checksum.substring(1,4);
        }
        tempdata=tempdata.splice(0,tempdata.length-1);//удалить элемент массива (контрольную сумму,OD,0A)
        data=tempdata.toString();//получить строку без '$' и контрольной суммы
        calc_checksum = calc_xor(data);
        calc_checksum = calc_xor(data).toString(16).toUpperCase()+"\r\n";//получить контрольную сумму методом побайтового XOR, преобразовать в hex
        if (calc_checksum==input_checksum)
        {
            format_data(tempdata, remoteAddress, remotePort);
            if (tempdata[0]!="SIC")
            {
                for (i = 0; i < client_conn_table.length; i++) {
                    arr = client_conn_table[i];
                    if (arr.Port == stream.remotePort && arr.IP == stream.remoteAddress && arr.Customer_ID == "await" && parsed_data != "incorrect data" && parsed_data != "bad checksum")//если есть такой адрес в таблице подключений и он ожидает назначение Customer_ID
                    {
                        arr.Customer_ID = parsed_data[1];
                        client_conn_table[i] = arr;
                    }
                }
            }
            // return tempdata;
        }
        else
        {
            console.log('bad checksum')
            // return "bad checksum";
        }
        // return calc_checksum;
    }

}


/*********************************************************************
 * Function:         calc_xor(stringdata)
 *
 * PreCondition:     none
 *
 * Input:            string
 * Return^           XOR by byte.
 *
 * Overview:         This function calc XOR (checsum)
 *
 * Note:            input MS,0111111111,3.1 return ACSI(M)^ACSI(S)^ACSI(0)^ACSI(1)^ACSI(1)^ACSI(1)^ACSI(1)^ACSI(1)^ACSI(1)^ACSI(1)^ACSI(1)^ACSI(1)^ACSI(3)^ACSI(.)^ACSI(1)
 ********************************************************************/
function calc_xor(stringdata)
{
    var stringdata=stringdata;
    var calc_xor_checksum;
    var tempdata = new Array();
    tempdata = stringdata.split(',');//преобразовать в массив по предложениям
    var temp=new Array();
    var temp_checksum=new Array();
    var i;
    for (i=0;i<tempdata.length;i++)
    {
        temp=tempdata[i].split('');//разбить каждый элемент массива побайтно
        temp.push(',');
        switch (temp.length)
        {
            case 0:
                return false;
                break
            case 1:
                temp_checksum[i]=temp[0].charCodeAt(0);
                break
            case 2:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0);
                break
            case 3:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0);
                break
            case 4:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0);
                break
            case 5:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0);
                break
            case 6:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0);
                break
            case 7:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0);
                break
            case 8:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0);
                break
            case 9:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0);
                break
            case 10:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0);
                break
            case 11:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0);
                break
            case 12:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0)^temp[11].charCodeAt(0);
                break
            case 13:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0)^temp[11].charCodeAt(0)^temp[12].charCodeAt(0);
                break
            case 14:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0)^temp[11].charCodeAt(0)^temp[12].charCodeAt(0)^temp[13].charCodeAt(0);
                break
            case 15:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0)^temp[11].charCodeAt(0)^temp[12].charCodeAt(0)^temp[13].charCodeAt(0)^temp[14].charCodeAt(0);
                break
            case 16:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0)^temp[11].charCodeAt(0)^temp[12].charCodeAt(0)^temp[13].charCodeAt(0)^temp[14].charCodeAt(0)^temp[15].charCodeAt(0);
                break
            case 17:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0)^temp[11].charCodeAt(0)^temp[12].charCodeAt(0)^temp[13].charCodeAt(0)^temp[14].charCodeAt(0)^temp[15].charCodeAt(0)^temp[16].charCodeAt(0);
                break
            case 18:
                temp_checksum[i]=temp[0].charCodeAt(0)^temp[1].charCodeAt(0)^temp[2].charCodeAt(0)^temp[3].charCodeAt(0)^temp[4].charCodeAt(0)^temp[5].charCodeAt(0)^temp[6].charCodeAt(0)^temp[7].charCodeAt(0)^temp[8].charCodeAt(0)^temp[9].charCodeAt(0)^temp[10].charCodeAt(0)^temp[11].charCodeAt(0)^temp[12].charCodeAt(0)^temp[13].charCodeAt(0)^temp[14].charCodeAt(0)^temp[15].charCodeAt(0)^temp[16].charCodeAt(0)^temp[17].charCodeAt(0);
                break
            default: break
        }
    }
    for (i=0;i<temp_checksum.length;i++)
    {
        if (calc_xor_checksum==undefined)
        {
            calc_xor_checksum=temp_checksum[i];
        }
        else
        {
            calc_xor_checksum=calc_xor_checksum^temp_checksum[i];
        }
    }
    return calc_xor_checksum;
}


/*********************************************************************
 * Function:         format_data(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed and input tcp checked data
 *
 * Input:            Array
 * Return^           class formated_data
 *
 * Overview:         This function covvert Array to class data_MYSQL
 *
 * Note:
 ********************************************************************/
function format_data(tempdata, remoteAddress, remotePort)
{
    switch (tempdata[0])
    {
        case "V"://Напряжение слейва
            processV(tempdata, remoteAddress, remotePort);
            break
        case "T"://Температура
            processT(tempdata, remoteAddress, remotePort);
            break
        case "H"://Влажность
            processH(tempdata, remoteAddress, remotePort);
            break
        case "R"://Состояние релейного выхода
            processR(tempdata, remoteAddress, remotePort);
            break
        case "DO"://Слейв offline
            processDO(tempdata, remoteAddress, remotePort);
            break
        case "SO"://Датчик ds18b20 offline
            processSO(tempdata, remoteAddress, remotePort);
            break
        case "SE"://Датчики ds18b20 отсутствуют или КЗ/обрыв на линии/линиях
            processSE(tempdata, remoteAddress, remotePort);
            break
        case "SIM"://ID мастера (по запросу tcp server "MIS")
            processSIM(tempdata, remoteAddress, remotePort);
            break
        case "SLC"://
            processSLC(tempdata, remoteAddress, remotePort);
            break
        case "STC"://Получить лог-показания одного 18b20
            processSTC(tempdata, remoteAddress, remotePort);
            break
        case "SHC"://Получить лог-показания одного датчика влажности
            processSHC(tempdata, remoteAddress, remotePort);
            break
        case "SRC"://Получить лог коммутации блока реле
            processSRC(tempdata, remoteAddress, remotePort);
            break
        case "SMC"://Получить лог мастера
            processSMC(tempdata, remoteAddress, remotePort);
            break
        case "SIC"://ID клиента
            processSIC(tempdata, remoteAddress, remotePort);
            break
        case "SDC"://задать время хранения лога по Customer_ID
            processSDC(tempdata, remoteAddress, remotePort);
            break
        case "MRC"://установить выход реле (tcp Клиент-Мастеру)
            processMRC(tempdata, remoteAddress, remotePort);
            break
        case "MSC"://Установить время сна (tcp Клиент-Мастеру)
            processMSC(tempdata, remoteAddress, remotePort);
            break
        case "MTC"://Запрос мастеру: Получить сохраненный список слейвов
            processMTC(tempdata, remoteAddress, remotePort);
            break
        case "CTM"://Ответ мастера: адрес слейва (один слейв - одно сообщение)
            processCTM(tempdata, remoteAddress, remotePort);
            break
        case "MFC"://Запрос мастеру: Включить поиск новых слейвов
            processMFC(tempdata, remoteAddress, remotePort);
            break
        case "CFM"://Ответ мастера: Добавлен новый слейв (один слейв - одно сообщение)
            processCFM(tempdata, remoteAddress, remotePort);
            break
        case "MDC"://Запрос мастеру: Удалить слейв (один слейв - одно сообщение)
            processMDC(tempdata, remoteAddress, remotePort);
            break
        case "CDM"://Ответ мастера: Удален слейв (один слейв - одно сообщение)
            processCDM(tempdata, remoteAddress, remotePort);
            break
        default: break
    }
}


/*********************************************************************
 * Function:         check_log_datum(temp_date_log, temp_time_log, tempdata)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            temp_date_log - date of log field, temp_date_log - time of log field
 * Return^           true of false
 *
 * Overview:         This function check correct data and time of log field
 *
 * Note:             if correct - return true
 ********************************************************************/
function check_log_datum(temp_date_log, temp_time_log, tempdata)
{
    temp_date_log.year=temp_date_log[0];
    temp_date_log.month=temp_date_log[1];
    temp_date_log.day=temp_date_log[2];
    temp_time_log.hour=temp_time_log[0];
    temp_time_log.minute=temp_time_log[1];
    temp_date_log.second=temp_time_log[2];
    temp_date_first_input=tempdata[3].split('-');
    if (tempdata[0]=="SMC")
    {
        temp_date_first_input=tempdata[2].split('-');
    }
    temp_date_first_input.year=temp_date_first_input[0];
    temp_date_first_input.month=temp_date_first_input[1];
    temp_date_first_input.day=temp_date_first_input[2];
    temp_date_last_input=tempdata[4].split('-');
    if (tempdata[0]=="SMC")
    {
        temp_date_last_input=tempdata[3].split('-');
    }
    temp_date_last_input.year=temp_date_last_input[0];
    temp_date_last_input.month=temp_date_last_input[1];
    temp_date_last_input.day=temp_date_last_input[2];
    temp_time_first_input=tempdata[5].split(':');
    if (tempdata[0]=="SMC")
    {
        temp_time_first_input=tempdata[4].split(':');
    }
    temp_time_first_input.hour=temp_time_first_input[0];
    temp_time_first_input.minute=temp_time_first_input[1];
    temp_time_first_input.second=temp_time_first_input[2];
    if (tempdata[0]=="SMC")
    {
        temp_time_last_input=tempdata[5].split(':');
    }
    else
    {
        temp_time_last_input=tempdata[6].split(':');
    }
    temp_time_last_input.hour=temp_time_last_input[0];
    temp_time_last_input.minute=temp_time_last_input[1];
    temp_time_last_input.second=temp_time_last_input[2];

    if(temp_time_log.hour=="00")
    {
        temp_date_log.hour=0;
    }
    if(temp_time_first_input.hour=="00")
    {
        temp_time_first_input.hour=0;
    }
    if(temp_time_last_input.hour=="00")
    {
        temp_time_last_input.hour=0;
    }

    //1. проверка года
    //1.1. Год события в промежутке нач-кон?
    if(temp_date_log.year>temp_date_first_input.year && temp_date_log.year<temp_date_last_input.year)//случай, если год события в промежутке
    {
        return true;
    }
    //1.2 Год события=начальному году в запросе и нач. год запроса!=конечному
    if(temp_date_log.year==temp_date_first_input.year && temp_date_first_input.year!=temp_date_last_input.year)
    {
        //1.2.1 Месяц события > начального месяца в запросе
        if(temp_date_log.month>temp_date_first_input.month)
        {
            return true;
        }
        //1.2.2 Месяц события < начального месяца в запросе
        if(temp_date_log.month<temp_date_first_input.month)
        {
            return false;
        }
        //1.2.3 Месяц события =  начальному месяцу в запросе
        if(temp_date_log.month==temp_date_first_input.month)
        {
            //1.2.3.1 День события >  начального в запросе
            if(temp_date_log.day>temp_date_first_input.day)
            {
                return true;
            }
            //1.2.3.2 День события <  начального в запросе
            if(temp_date_log.day<temp_date_first_input.day)
            {
                return false;
            }
            //1.2.3.3 День события =  начальному в запросе
            if(temp_date_log.day==temp_date_first_input.day)
            {
                //1.2.3.3.1 Час события >  начального часа в запросе
                if(temp_time_log.hour>temp_time_first_input.hour)
                {
                    return true;
                }
                //1.2.3.3.2 Час события <  начального часа в запросе
                if(temp_time_log.hour<temp_time_first_input.hour)
                {
                    return false;
                }
                //1.2.3.3.3 Час события =  начальному в запросе
                if(temp_time_log.hour==temp_time_first_input.hour)
                {
                    return true;
                }
            }
        }
    }
    //1.3 Год события=конечному году в запросе и кон. год запроса!=начальному
    if(temp_date_log.year==temp_date_last_input.year && temp_date_last_input.year!=temp_date_first_input.year)
    {
        //1.3.1 Месяц события > конечного месяца в запросе
        if(temp_date_log.month>temp_date_last_input.month)
        {
            return false;
        }
        //1.3.2 Месяц события < конечного месяца в запросе
        if(temp_date_log.month<temp_date_last_input.month)
        {
            return true;
        }
        //1.3.3 Месяц события =  конечному месяцу в запросе
        if(temp_date_log.month==temp_date_last_input.month)
        {
            //1.3.3.1 День события >  конечного дня в запросе
            if(temp_date_log.day>temp_date_false_input.day)
            {
                return false;
            }
            //1.3.3.2 День события <  конечного дня в запросе
            if(temp_date_log.day<temp_date_last_input.day)
            {
                return true;
            }
            //1.3.3.3 День события =  конечному дню в запросе
            if(temp_date_log.day==temp_date_last_input.day)
            {
                //1.3.3.3.1 Час события >  конечного часа в запросе
                if(temp_time_log.hour>temp_time_first_input.hour)
                {
                    return false;
                }
                //1.3.3.3.2 Час события <  конечного часа в запросе
                if(temp_time_log.hour<temp_time_last_input.hour)
                {
                    return true;
                }
                //1.3.3.3.3 Час события =  конечному часу в запросе
                if(temp_time_log.hour==temp_time_last_input.hour)
                {
                    return true;
                }
            }
        }
    }
    //1.4 Год события=конечному году в запросе и нач. год запроса=кон.году запроса
    if(temp_date_log.year==temp_date_last_input.year && temp_date_last_input.year==temp_date_first_input.year)
    {
        //1.4.1 Месяц события в промежутке нач-кон?
        if(temp_date_log.month>temp_date_first_input.month && temp_date_log.month<temp_date_last_input.month)
        {
            return true;
        }
        //1.4.2 Месяц события < начального месяца в запросе или > конечного месяца в запросе
        if(temp_date_log.month<temp_date_first_input.month || temp_date_log.month>temp_date_last_input.month)
        {
            return false;
        }
        //1.4.3 Месяц события =  начальному месяцу в запросе и != конечному месяцу в запросе
        if(temp_date_log.month==temp_date_first_input.month && temp_date_log.month!=temp_date_last_input.month)
        {
            //1.4.3.1 День события > начального дня в запросе?
            if(temp_date_log.day>temp_date_first_input.day)
            {
                return true;
            }
            //1.4.3.2 День события < начального дня в запросе
            if(temp_date_log.day<temp_date_first_input.day)
            {
                return false;
            }
            //1.4.3.3 День события = начальному дню в запросе
            if(temp_date_log.day==temp_date_first_input.day)
            {
                //1.4.3.3.1 Час события >= начального часа в запросе?
                if(temp_time_log.hour>=temp_time_first_input.hour)
                {
                    return true;
                }
                //1.4.3.3.2 Час события < начального часа в запросе?
                if(temp_time_log.hour<temp_time_first_input.hour)
                {
                    return false;
                }
            }
        }
        //1.4.4 Месяц события =  конечному месяцу в запросе и != начальному месяцу в запросе
        if(temp_date_log.month==temp_date_last_input.month && temp_date_log.month!=temp_date_first_input.month)
        {
            //1.4.4.1 День события < конечного дня в запросе?
            if(temp_date_log.day<temp_date_last_input.day)
            {
                return true;
            }
            //1.4.4.2 День события > конечного дня в запросе
            if(temp_date_log.day>temp_date_last_input.day)
            {
                return false;
            }
            //1.4.4.3 День события = конечному дню в запросе
            if(temp_date_log.day==temp_date_last_input.day)
            {
                //1.4.4.3.1 Час события <= конечного часа в запросе?
                if(temp_time_log.hour<=temp_time_last_input.hour)
                {
                    return true;
                }
                //1.4.4.3.2 Час события > конечного часа в запросе?
                if(temp_time_log.hour>temp_time_last_input.hour)
                {
                    return false;
                }
            }
        }
        //1.4.5 Месяц события =  конечному месяцу в запросе и = начальному месяцу в запросе
        if(temp_date_log.month==temp_date_last_input.month && temp_date_log.month==temp_date_first_input.month)
        {
            //1.4.5.1 День события в промежутке?
            if(temp_date_log.day>temp_date_first_input.day && temp_date_log.day<temp_date_last_input.day)
            {
                return true;
            }
            //1.4.5.2 День события < начального дня в запросе или > конечного
            if(temp_date_log.day<temp_date_first_input.day || temp_date_log.day>temp_date_last_input.day )
            {
                return false;
            }
            //1.4.5.3 День события = начальному дню в запросе и != конечному
            if(temp_date_log.day==temp_date_first_input.day && temp_date_log.day!=temp_date_last_input.day )
            {
                //1.4.5.3.1 Час события >= начального часа в запросе?
                if(temp_time_log.hour>=temp_time_first_input.hour)
                {
                    return true;
                }
                //1.4.5.3.2 Час события < начального часа в запросе?
                if(temp_time_log.hour<temp_time_first_input.hour)
                {
                    return false;
                }
            }
            //1.4.5.4 День события = конечному дню в запросе и != начальному
            if(temp_date_log.day==temp_date_last_input.day && temp_date_log.day!=temp_date_first_input.day )
            {
                //1.4.5.4.1 Час события <= конечного часа в запросе?
                if(temp_time_log.hour<=temp_time_last_input.hour)
                {
                    return true;
                }
                //1.4.5.4.2 Час события > конечного часа в запросе?
                if(temp_time_log.hour>temp_time_last_input.hour)
                {
                    return false;
                }
            }
            //1.4.5.5 День события = конечному дню в запросе и = начальному дню в запросе
            if (temp_date_log.day==temp_date_first_input.day && temp_date_log.day==temp_date_last_input.day)
            {
                //1.4.5.5.1 Час события в промежутке?
                if(temp_time_log.hour>temp_time_first_input.hour && temp_time_log.hour<temp_time_last_input.hour)
                {
                    return true;
                }
                //1.4.5.5.2 Час события < начального часа в запросе или > конечного
                if(temp_time_log.hour<temp_time_first_input.hour || temp_time_log.day>temp_time_last_input.hour )
                {
                    return false;
                }
                //1.4.5.5.3 Час события = начальному часу в запросе и != конечному
                if(temp_time_log.hour==temp_time_first_input.hour && temp_time_log.hour!=temp_time_last_input.hour)
                {
                    return true;
                }
                //1.4.5.5.4 Час события = конечному часу в запросе и != начальному
                if(temp_time_log.hour==temp_time_last_input.hour && temp_time_log.hour!=temp_time_first_input.hour )
                {
                    return true;
                }
                //1.4.5.5.5 Час события = конечному часу в запросе и = начальному часу в запросе
                if (temp_time_log.hour==temp_time_first_input.hour && temp_time_log.hour==temp_time_last_input.hour)
                {
                    return true;
                }
            }
        }
    }
}


/*********************************************************************
 * Function:         processV(tempdata)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:
 ********************************************************************/
function processV(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            myNumber=rows;
            if (err) throw err;
            callback.call(myNumber);
        });
    }

    var i=0;
    var k=0;
    var n=0;
    check_table(function () {
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$V: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$V: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                        var Master_ID=tempdata[1];
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var Event=tempdata[3];
                                var Battery_V=tempdata[4];
                                var Slave_ID=this[n].Slave_ID;
                                connection.query("INSERT INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Battery_V, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Event +"','"+ Battery_V +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать значение напряжения в лог
                                connection.query("UPDATE `slave_devices` SET `Master_ID` =('"+ Master_ID +"') WHERE  `slave_devices`.`Slave_ID` ='"+ Slave_ID +"';");//обновить MASTER_ID для этого слейва (вдруг поменяли мастера)
                                connection.query("UPDATE `slave_devices` SET `Battery_V` =('"+ Battery_V +"') WHERE  `slave_devices`.`Slave_ID` ='"+ Slave_ID +"';");//обновить напряжение батареи для этого слейва
                                connection.query("UPDATE `slave_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `slave_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                                connection.query("UPDATE `slave_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `slave_devices`.`SLAVE_ID` ='"+ Slave_ID +"';");//обновить время
                                console.log("$V: Slave device with ID: " + Slave_ID + " already exist in slave_devices!... datum updated!");
                                var z;
                                var h;
                                var arr;
                                for (z = 0; z < master_conn_table.length; z++)
                                {
                                    arr = master_conn_table[z];
                                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table[z] = arr;
                                        break;
                                    }
                                }
                                for (z = 0; z < master_conn_table_usb.length; z++)
                                {
                                    arr = master_conn_table_usb[z];
                                    if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table_usb[z] = arr;
                                        break;
                                    }
                                }
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='V'+','+Master_ID+','+Slave_ID+','+Event+','+Battery_V;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'V'+','+Master_ID+','+Slave_ID+','+Event+','+Battery_V+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0)//совпадений не найдено - вставить поле с новым Slave_ID в таблицу slave_devices, вставить дату и время
                        {
                            var Battery_V=tempdata[4];
                            var Event=tempdata[3];
                            var Slave_ID = tempdata[2];
                            connection.query("INSERT INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Battery_V, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_ON','"+ Battery_V +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать значение напряжения в лог
                            connection.query("INSERT INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Battery_V, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Event +"','"+ Battery_V +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать значение напряжения в лог
                            connection.query("INSERT INTO `slave_devices`(Customer_ID, Master_ID, Slave_ID, Battery_V) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Battery_V + "');");
                            connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить дату
                            connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить время
                            console.log("$V: Slave device field with ID: " + Slave_ID + " create in 'slave_devices'!");
                            var z;
                            var arr;
                            var h;
                            for (z = 0; z < master_conn_table.length; z++)
                            {
                                arr = master_conn_table[z];
                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table[z] = arr;
                                    break;
                                }
                            }
                            for (z = 0; z < master_conn_table_usb.length; z++)
                            {
                                arr = master_conn_table_usb[z];
                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table_usb[z] = arr;
                                    break;
                                }
                            }
                            for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                            {
                                arr = client_conn_table[h];
                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                {
                                    streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='V'+','+Master_ID+','+Slave_ID+','+Event+','+Battery_V;
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'V'+','+Master_ID+','+Slave_ID+','+Event+','+Battery_V+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("$V: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processT(tempdata)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:
 ********************************************************************/
function processT(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            myNumber=rows;
            if (err) throw err;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время
                        {
                            var Master_ID=this[n].Master_ID;
                            j=1;
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$T: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$T: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                        var Master_ID=tempdata[1];
                    }
                    strQuery=("SELECT Customer_ID, Master_ID, Slave_ID, Sleep_time, Name FROM slaves_setting");//зарегистрирован ли такой SENSOR в sensors_setting?
                    check_table(function () {
                        for (g = 0; g < this.length; g++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[g].Master_ID == tempdata[1] && this[g].Slave_ID == tempdata[2]) {
                                var Slave_Name = this[g].Name;
                                var Sleep_Time = this[g].Sleep_time;
                            }
                        }
                        strQuery = ("SELECT Slave_ID, Name, Sleep_time FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                        check_table(function () {
                            var j = 0;
                            for (n = 0; n < this.length; n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                            {
                                if (this[n].Slave_ID == tempdata[2]) {
                                    j = 1;
                                    var Slave_ID = this[n].Slave_ID;
                                    if (Slave_Name != undefined && Slave_Name != this[n].Name) {
                                        connection.query("UPDATE `slave_devices` SET `Name` =('" + Slave_Name + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                    }
                                    if (Sleep_Time != undefined && Sleep_Time != this[n].Sleep_Time) {
                                        connection.query("UPDATE `slave_devices` SET `Sleep_Time` =('" + Sleep_Time + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                    }
                                    connection.query("UPDATE `slave_devices` SET `Master_ID` =('" + Master_ID + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить MASTER_ID для этого слейва (вдруг поменяли мастера)
                                    connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Master_ID` ='" + Master_ID + "';");//обновить дату
                                    connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`SLAVE_ID` ='" + Slave_ID + "';");//обновить время
                                    console.log("$T: Slave device with ID: " + Slave_ID + " already exist in slave_devices!... datum updated!");
                                }
                            }
                            if (j == 0)//совпадений не найдено - вставить поле с новым Slave_ID в таблицу master_devices, вставить дату и время
                            {
                                var Slave_ID = tempdata[2];
                                connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','S_ON','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "');");//записать событие "обнаружен новый Master_device!" в лог
                                connection.query("INSERT INTO `slave_devices`(Customer_ID, Master_ID, Slave_ID) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + tempdata[2] + "');");
                                connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Slave_ID` ='" + tempdata[2] + "';");//обновить дату
                                connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`Slave_ID` ='" + tempdata[2] + "';");//обновить время
                                // if (Slave_Name != undefined) {
                                //     connection.query("UPDATE `slave_devices` SET `Name` =('" + Slave_Name + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                // }
                                // if (Sleep_Time != undefined) {
                                //     connection.query("UPDATE `slave_devices` SET `Sleep_Time` =('" + Sleep_Time + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                // }
                                console.log("$T: Slave device field with ID: " + Slave_ID + " create in 'slave_devices'!");
                            }
                            strQuery = ("SELECT Customer_ID, Master_ID, Slave_ID, Address, Name, Upper_Value, Lower_Value, Delta FROM sensors_setting");//зарегистрирован ли такой SENSOR в sensors_setting?
                            check_table(function () {
                                for (z = 0; z < this.length; z++)//такой сенсор уже существует в таблице sensors_setting?
                                {
                                    if (this[z].Master_ID == tempdata[1] && this[z].Slave_ID == tempdata[2] && this[z].Address == tempdata[3]) {
                                        var Name = this[z].Name;
                                        var Lower_Value = this[z].Lower_Value;
                                        var Upper_Value = this[z].Upper_Value;
                                        var Delta = this[z].Upper_Value;
                                        tempdata["Name"] = Name;
                                        tempdata["Lower_Value"] = Lower_Value;
                                        tempdata["Upper_Value"] = Upper_Value;
                                        tempdata["Delta"] = Delta;
                                        break;
                                    }
                                }
                                strQuery = ("SELECT Type, Slave_ID, Address, Name, Alarm_Status, Lower_Value, Upper_Value, Delta FROM sensors");//поиск всех датчиков в таблице sensors
                                check_table(function () {
                                    j = 0;
                                    for (n = 0; n < this.length; n++)//ds18b20 с таким адресом уже существует в таблице sensors?
                                    {
                                        var n = n;
                                        if (this[n].Type == 'ds18b20' && this[n].Slave_ID == tempdata[2] && this[n].Address == tempdata[3]) {
                                            j = 1;
                                            var Delta = this[n].Delta;
                                            if (Delta == "" && tempdata.Delta != undefined){
                                                Delta = tempdata.Delta;
                                            }
                                            var Lower_Value = this[n].Lower_Value;
                                            if (Lower_Value == "" && tempdata.Lower_Value != undefined){
                                                Lower_Value = tempdata.Lower_Value;
                                            }
                                            var Upper_Value = this[n].Upper_Value;
                                            if (Upper_Value == "" && tempdata.Upper_Value != undefined){
                                                Upper_Value = tempdata.Upper_Value;
                                            }
                                            var Name = this[n].Name;
                                            if (Name == "" && tempdata.Name != undefined){
                                                Name = tempdata.Name;
                                            }
                                            var Alarm_Status = this[n].Alarm_Status;
                                            if (Alarm_Status == "" && tempdata.Alarm_Status != undefined){
                                                Alarm_Status = tempdata.Alarm_Status;
                                            }
                                            var Sensor_addr = ' ';
                                            var T_data = tempdata[4];
                                            var Type = "ds18b20";
                                            var Sensor_addr = tempdata[3];
                                            connection.query("UPDATE `sensors` SET `Master_ID` =('" + Master_ID + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить Master_ID для этого датчика (вдруг поменяли мастера)
                                            connection.query("UPDATE `sensors` SET `Slave_ID` =('" + Slave_ID + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить Slave_ID для этого датчика (вдруг поменяли слейв)
                                            connection.query("UPDATE `sensors` SET `Value` =('" + T_data + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить значение температуры для этого датчика
                                            connection.query("UPDATE `sensors` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить дату
                                            connection.query("UPDATE `sensors` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить время
                                            connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time, Lower_Value, Upper_Value, Alarm_Status, Name, Delta) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','0','" + T_data + "','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + " ','" + Lower_Value + "','" + Upper_Value +"','"+ Alarm_Status +"','"+ Name +"','"+ Delta +"');");//записать событие  в лог
                                            //
                                            // connection.query("UPDATE LOW_PRIORITY `sensors` SET `Master_ID` =('" + Master_ID + "') WHERE  `sensors`.`Address` ='" + Sensor_addr + "';");//обновить Master_ID для этого датчика (вдруг поменяли мастера)
                                            // connection.query("UPDATE LOW_PRIORITY `sensors` SET `Slave_ID` =('" + Slave_ID + "') WHERE  `sensors`.`Address` ='" + Sensor_addr + "';");//обновить Slave_ID для этого датчика (вдруг поменяли слейв)
                                            // connection.query("UPDATE LOW_PRIORITY `sensors` SET `Value` =('" + T_data + "') WHERE  `sensors`.`Address` ='" + Sensor_addr + "';");//обновить значение температуры для этого датчика
                                            // connection.query("UPDATE LOW_PRIORITY `sensors` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `sensors`.`Address` ='" + Sensor_addr + "';");//обновить дату
                                            // connection.query("UPDATE LOW_PRIORITY `sensors` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `sensors`.`Address` ='" + Sensor_addr + "';");//обновить время
                                            // connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','0','" + T_data + "','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "');");//записать событие "S_ON" в лог                                    console.log("Temp Sensor with Address: " + Sensor_addr + " already exist in sensors!... value and datum updated!");
                                            // connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time, Lower_Value, Upper_Value, Alarm_Status, Name, Delta) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','0','" + T_data + "','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + " ','" + Lower_Value + "','" + Upper_Value +"','"+ Alarm_Status +"','"+ Name +"','"+ Delta +"');");//записать событие  в лог
                                            var z;
                                            var arr;
                                            var h;
                                            for (z = 0; z < master_conn_table.length; z++) {
                                                arr = master_conn_table[z];
                                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                                {
                                                    arr.Master_ID = Master_ID;
                                                    master_conn_table[z] = arr;
                                                    break;
                                                }
                                            }
                                            for (z = 0; z < master_conn_table_usb.length; z++) {
                                                arr = master_conn_table_usb[z];
                                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                                {
                                                    arr.Master_ID = Master_ID;
                                                    master_conn_table_usb[z] = arr;
                                                    break;
                                                }
                                            }
                                            for (h = 0; h < client_conn_table.length; h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                            {
                                                arr = client_conn_table[h];
                                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                                {
                                                    streams.forEach(function (stream) {           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                                        if (arr.IP == stream.remoteAddress && arr.Port == stream.remotePort) {
                                                            var data = 'T' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data;
                                                            var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                                            data = '$' + 'T' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                                            stream.write(data, 'utf8');
                                                        }
                                                    });
                                                }
                                            }
                                            // break;
                                        }
                                    }
                                    if (j == 0)//совпадений не найдено - вставить поле с новым type=18b20 в таблицу sensors, вставить дату и время, добавить в лог sensors_log событие S_ON
                                    {
                                        var T_data = tempdata[4];
                                        var Type = "ds18b20";
                                        var Sensor_addr = tempdata[3];
                                        connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','H_ON','" + T_data + "','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "');");//записать событие "H_ON" в лог
                                        connection.query("INSERT INTO `sensors`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','" + T_data + "','" + "');");
                                        connection.query("UPDATE `sensors` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `sensors`.`Type` ='" + Type + "';");//обновить дату
                                        connection.query("UPDATE `sensors` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `sensors`.`Type` ='" + Type + "';");//обновить время
                                        if (Name != undefined) {
                                            connection.query("UPDATE `sensors` SET `Name` =('"+Name+"') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Name для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Lower_Value` =('" + Lower_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Lower_value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Upper_Value` =('" + Upper_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Upper_value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Delta` =('" + Delta + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Name для этого датчика (если существует)
                                        }
                                        else{
                                            Delta = tempdata.Delta;
                                            Lower_Value = tempdata.Lower_Value;
                                            Upper_Value = tempdata.Upper_Value;
                                            Name = tempdata.Name;
                                            Alarm_Status = tempdata.Alarm_Status;
                                            connection.query("UPDATE `sensors` SET `Name` =('"+Name+"') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Name для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Lower_Value` =('" + Lower_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Lower_Value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Upper_Value` =('" + Upper_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Upper_Value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Upper_Value` =('" + Delta + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Delta для этого датчика (если существует)
                                        }
                                        var z;
                                        var arr;
                                        var h;
                                        for (z = 0; z < master_conn_table.length; z++) {
                                            arr = master_conn_table[z];
                                            if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                            {
                                                arr.Master_ID = Master_ID;
                                                master_conn_table[z] = arr;
                                                break;
                                            }
                                        }
                                        for (z = 0; z < master_conn_table_usb.length; z++) {
                                            arr = master_conn_table_usb[z];
                                            if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                            {
                                                arr.Master_ID = Master_ID;
                                                master_conn_table_usb[z] = arr;
                                                break;
                                            }
                                        }
                                        for (h = 0; h < client_conn_table.length; h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                        {
                                            arr = client_conn_table[h];
                                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                            {
                                                streams.forEach(function (stream) {           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                                    if (arr.IP == stream.remoteAddress && arr.Port == stream.remotePort) {
                                                        var data = 'T' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data;
                                                        var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                                        data = '$' + 'T' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                                        stream.write(data, 'utf8');
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            });
                        });
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("$T: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processH(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:
 ********************************************************************/
function processH(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            myNumber=rows;
            if (err) throw err;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        var n=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$H: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$H: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                        var Master_ID=tempdata[1];
                    }
                    strQuery=("SELECT Customer_ID, Master_ID, Slave_ID, Sleep_time, Name FROM slaves_setting");//зарегистрирован ли такой SENSOR в sensors_setting?
                    check_table(function () {
                        for (g = 0; g < this.length; g++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[g].Master_ID == tempdata[1] && this[g].Slave_ID == tempdata[2]) {
                                var Slave_Name = this[g].Name;
                                var Sleep_Time = this[g].Sleep_time;
                            }
                        }
                        strQuery = ("SELECT Slave_ID, Name, Sleep_time FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                        check_table(function () {
                            j = 0;
                            for (n = 0; n < this.length; n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                            {
                                if (this[n].Slave_ID == tempdata[2]) {
                                    j = 1;
                                    var Slave_ID = this[n].Slave_ID;
                                    // if (Slave_Name != undefined && Slave_Name != this[n].Name) {
                                    //     connection.query("UPDATE `slave_devices` SET `Name` =('" + Slave_Name + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                    // }
                                    // if (Sleep_Time != undefined && Sleep_Time != this[n].Sleep_Time) {
                                    //     connection.query("UPDATE `slave_devices` SET `Sleep_Time` =('" + Sleep_Time + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                    // }
                                    connection.query("UPDATE `slave_devices` SET `Master_ID` =('" + Master_ID + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить MASTER_ID для этого слейва (вдруг поменяли мастера)
                                    connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Master_ID` ='" + Slave_ID + "';");//обновить дату
                                    connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`SLAVE_ID` ='" + Slave_ID + "';");//обновить время
                                    console.log("$H: Slave device with ID: " + Slave_ID + " already exist in slave_devices!... datum updated!");
                                }
                            }
                            if (j == 0)//совпадений не найдено - вставить поле с новым Slave_ID в таблицу master_devices, вставить дату и время
                            {
                                var Slave_ID = tempdata[2];
                                connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','S_ON','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "');");//записать событие "обнаружен новый Master_device!" в лог
                                connection.query("INSERT INTO `slave_devices`(Customer_ID, Master_ID, Slave_ID) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "');");
                                connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить дату
                                connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить время
                                // if (Slave_Name != undefined) {
                                //     connection.query("UPDATE `slave_devices` SET `Name` =('" + Slave_Name + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                // }
                                // if (Sleep_Time != undefined) {
                                //     connection.query("UPDATE `slave_devices` SET `Sleep_Time` =('" + Sleep_Time + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить Name для этого Slave (если существует)
                                // }
                                console.log("$H: Slave device field with ID: " + Slave_ID + " create in 'slave_devices'!");
                            }
                            strQuery = ("SELECT Customer_ID, Master_ID, Slave_ID, Address, Name, Upper_Value, Lower_Value, Delta FROM sensors_setting");//зарегистрирован ли такой SENSOR в sensors_setting?
                            check_table(function () {
                                for (z = 0; z < this.length; z++)//такой сенсор уже существует в таблице sensors_setting?
                                {
                                    if (this[z].Master_ID == tempdata[1] && this[z].Slave_ID == tempdata[2] && this[z].Address == tempdata[3]) {
                                        var Name = this[z].Name;
                                        var Lower_Value = this[z].Lower_Value;
                                        var Upper_Value = this[z].Upper_Value;
                                        var Delta = this[z].Upper_Value;
                                        tempdata["Name"] = Name;
                                        tempdata["Lower_Value"] = Lower_Value;
                                        tempdata["Upper_Value"] = Upper_Value;
                                        tempdata["Delta"] = Delta;
                                        break;
                                    }
                                }
                                strQuery = ("SELECT Type, Slave_ID, Address, Name, Alarm_Status, Lower_Value, Upper_Value, Delta FROM sensors");//поиск всех датчиков в таблице sensors
                                check_table(function () {
                                    j = 0;
                                    for (n = 0; n < this.length; n++) {
                                        if (this[n].Type == 'H' && this[n].Slave_ID == tempdata[2] && this[n].Address == tempdata[3])//найден датчик влажности у конкретного слейва, обновить данные
                                        {
                                            j = 1;
                                            var Delta = this[n].Delta;
                                            if (Delta == "" && tempdata.Delta != undefined){
                                                Delta = tempdata.Delta;
                                            }
                                            var Lower_Value = this[n].Lower_Value;
                                            if (Lower_Value == "" && tempdata.Lower_Value != undefined){
                                                Lower_Value = tempdata.Lower_Value;
                                            }
                                            var Upper_Value = this[n].Upper_Value;
                                            if (Upper_Value == "" && tempdata.Upper_Value != undefined){
                                                Upper_Value = tempdata.Upper_Value;
                                            }
                                            var Name = this[n].Name;
                                            if (Name == "" && tempdata.Name != undefined){
                                                Name = tempdata.Name;
                                            }
                                            var Alarm_Status = this[n].Alarm_Status;
                                            if (Alarm_Status == "" && tempdata.Alarm_Status != undefined){
                                                Alarm_Status = tempdata.Alarm_Status;
                                            }
                                            var Sensor_addr = ' ';
                                            var T_data = tempdata[4];
                                            var Type = "H";
                                            var Sensor_addr = tempdata[3];
                                            connection.query("UPDATE `sensors` SET `Master_ID` =('" + Master_ID + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить Master_ID для этого датчика (вдруг поменяли мастера)
                                            connection.query("UPDATE `sensors` SET `Slave_ID` =('" + Slave_ID + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить Slave_ID для этого датчика (вдруг поменяли слейв)
                                            connection.query("UPDATE `sensors` SET `Value` =('" + T_data + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить значение температуры для этого датчика
                                            connection.query("UPDATE `sensors` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить дату
                                            connection.query("UPDATE `sensors` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Slave_ID` ='" + this[n].Slave_ID + "';");//обновить время
                                            connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time, Lower_Value, Upper_Value, Alarm_Status, Name, Delta) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','0','" + T_data + "','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + " ','" + Lower_Value + "','" + Upper_Value +"','"+ Alarm_Status +"','"+ Name +"','"+ Delta +"');");//записать событие  в лог
                                            console.log("$H: Hudmunity Sensor with Type: " + Type + " already exist in sensors!... value and datum updated!");
                                            var z;
                                            var arr;
                                            var h;
                                            for (z = 0; z < master_conn_table.length; z++) {
                                                arr = master_conn_table[z];
                                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                                {
                                                    arr.Master_ID = Master_ID;
                                                    master_conn_table[z] = arr;
                                                    break;
                                                }
                                            }
                                            for (z = 0; z < master_conn_table_usb.length; z++) {
                                                arr = master_conn_table_usb[z];
                                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                                {
                                                    arr.Master_ID = Master_ID;
                                                    master_conn_table_usb[z] = arr;
                                                    break;
                                                }
                                            }
                                            for (h = 0; h < client_conn_table.length; h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                            {
                                                arr = client_conn_table[h];
                                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                                {
                                                    streams.forEach(function (stream) {           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                                        if (arr.IP == stream.remoteAddress && arr.Port == stream.remotePort) {
                                                            var data = 'H' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data;
                                                            var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                                            data = '$' + 'H' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                                            stream.write(data, 'utf8');
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                    if (j == 0)//совпадений не найдено - вставить поле с новым type=H в таблицу sensors, вставить дату и время, добавить в лог sensors_log событие S_ON
                                    {
                                        var T_data = tempdata[4];
                                        var Type = "H";
                                        var Sensor_addr = tempdata[3];
                                        connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','H_ON','" + T_data + "','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "');");//записать событие "H_ON" в лог
                                        connection.query("INSERT INTO `sensors`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "','" + Sensor_addr + "','" + T_data + "','" + "');");
                                        connection.query("UPDATE `sensors` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `sensors`.`Type` ='" + Type + "';");//обновить дату
                                        connection.query("UPDATE `sensors` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `sensors`.`Type` ='" + Type + "';");//обновить время
                                        if (Name != undefined) {
                                            connection.query("UPDATE `sensors` SET `Name` =('"+Name+"') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Name для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Lower_Value` =('" + Lower_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Lower_value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Upper_Value` =('" + Upper_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Upper_value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Delta` =('" + Delta + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Name для этого датчика (если существует)
                                        }
                                        else{
                                            Delta = tempdata.Delta;
                                            Lower_Value = tempdata.Lower_Value;
                                            Upper_Value = tempdata.Upper_Value;
                                            Name = tempdata.Name;
                                            Alarm_Status = tempdata.Alarm_Status;
                                            connection.query("UPDATE `sensors` SET `Name` =('"+Name+"') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Name для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Lower_Value` =('" + Lower_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Lower_Value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Upper_Value` =('" + Upper_Value + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Upper_Value для этого датчика (если существует)
                                            connection.query("UPDATE `sensors` SET `Upper_Value` =('" + Delta + "') WHERE  `sensors`.`Type` ='" + Type + "' AND `sensors`.`Address` ='" + Sensor_addr + "' AND `sensors`.`Slave_ID` ='" + tempdata[2] + "';");//обновить Delta для этого датчика (если существует)
                                        }
                                        console.log("$H: Hudmunity Sensor with Type: " + Type + " create in 'sensors'!");
                                        var z;
                                        var arr;
                                        var h;
                                        for (z = 0; z < master_conn_table.length; z++) {
                                            arr = master_conn_table[z];
                                            if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                            {
                                                arr.Master_ID = Master_ID;
                                                master_conn_table[z] = arr;
                                                break;
                                            }
                                        }
                                        for (z = 0; z < master_conn_table_usb.length; z++) {
                                            arr = master_conn_table_usb[z];
                                            if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                            {
                                                arr.Master_ID = Master_ID;
                                                master_conn_table_usb[z] = arr;
                                                break;
                                            }
                                        }
                                        for (h = 0; h < client_conn_table.length; h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                        {
                                            arr = client_conn_table[h];
                                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                            {
                                                streams.forEach(function (stream) {           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                                    if (arr.IP == stream.remoteAddress && arr.Port == stream.remotePort) {
                                                        var data = 'H' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data;
                                                        var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                                        data = '$' + 'H' + ',' + Master_ID + ',' + Slave_ID + ',' + Sensor_addr + ',' + T_data + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                                        stream.write(data, 'utf8');
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            });
                        });
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("$H: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processR(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:
 ********************************************************************/
function processR(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$R: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$R: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                        var Master_ID=tempdata[1];
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var Slave_ID=this[n].Slave_ID;
                                connection.query("UPDATE `slave_devices` SET `Master_ID` =('"+ Master_ID +"') WHERE  `slave_devices`.`Slave_ID` ='"+ Slave_ID +"';");//обновить MASTER_ID для этого слейва (вдруг поменяли мастера)
                                connection.query("UPDATE `slave_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `slave_devices`.`Master_ID` ='"+ Slave_ID +"';");//обновить дату
                                connection.query("UPDATE `slave_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `slave_devices`.`SLAVE_ID` ='"+ Slave_ID +"';");//обновить время
                                console.log("$R: Slave device with ID: " + Slave_ID + " already exist in slave_devices!... datum updated!");
                            }
                        }
                        if (j==0)//совпадений не найдено - вставить поле с новым Slave_ID в таблицу master_devices, вставить дату и время
                        {
                            var Slave_ID = tempdata[2];
                            connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                            connection.query("INSERT INTO `slave_devices`(Customer_ID, Master_ID, Slave_ID) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "');");
                            connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить дату
                            connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить время
                            console.log("$R: Slave device field with ID: " + Slave_ID + " create in 'slave_devices'!");
                        }
                        strQuery=("SELECT Type, Address FROM sensors");//поиск всех реле в таблице sensors
                        check_table(function (){
                            j=0;
                            for (n=0;n<this.length;n++)
                            {
                                if (this[n].Type==tempdata[0] && this[n].Address==tempdata[3] && Slave_ID==tempdata[2])//найден блок реле у конкретного слейва, обновить данные
                                {
                                    j=1;
                                    var Sensor_addr=tempdata[3];//номер реле
                                    var T_data=tempdata[4];
                                    var Type="R";
                                    connection.query("UPDATE `sensors` SET `Master_ID` =('"+ Master_ID +"') WHERE  `sensors`.`Address` ='"+ Sensor_addr +"';");//обновить Master_ID для этого датчика (вдруг поменяли мастера)
                                    connection.query("UPDATE `sensors` SET `Slave_ID` =('"+ Slave_ID +"') WHERE  `sensors`.`Address` ='"+ Sensor_addr +"';");//обновить Slave_ID для этого датчика (вдруг поменяли слейв)
                                    connection.query("UPDATE `sensors` SET `Value` =('"+ T_data +"') WHERE  `sensors`.`Address` ='"+ Sensor_addr +"';");//обновить значение температуры для этого датчика
                                    connection.query("UPDATE `sensors` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `sensors`.`Address` ='"+ Sensor_addr +"';");//обновить дату
                                    connection.query("UPDATE `sensors` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `sensors`.`Address` ='"+ Sensor_addr +"';");//обновить время
                                    connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Type +"','"+ Sensor_addr +"','"+ T_data +"','"+ T_data +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать значение температуры в лог дял этого датчика
                                    console.log("$R: Relay Sensor, active relay № " + Sensor_addr + " already exist in sensors!... value, log and datum updated!");
                                    var z;
                                    var arr;
                                    var h;
                                    for (z = 0; z < master_conn_table.length; z++)
                                    {
                                        arr = master_conn_table[z];
                                        if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                        {
                                            arr.Master_ID = Master_ID;
                                            master_conn_table[z] = arr;
                                            break;
                                        }
                                    }
                                    for (z = 0; z < master_conn_table_usb.length; z++)
                                    {
                                        arr = master_conn_table_usb[z];
                                        if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                        {
                                            arr.Master_ID = Master_ID;
                                            master_conn_table_usb[z] = arr;
                                            break;
                                        }
                                    }
                                    for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                    {
                                        arr = client_conn_table[h];
                                        if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                        {
                                            streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                                if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                    var data='R'+','+Master_ID+','+Slave_ID+','+Sensor_addr+','+T_data;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'R'+','+Master_ID+','+Slave_ID+','+Sensor_addr+','+T_data+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            if (j==0)//совпадений не найдено - вставить поле с новым type=R в таблицу sensors, вставить дату и время, добавить в лог sensors_log событие S_ON
                            {
                                var Sensor_addr=tempdata[3];//номер реле
                                var T_data=tempdata[4];
                                var Type="R";
                                connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Type +"','"+ Sensor_addr +"',' R_ON','"+ T_data +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "S_ON" в лог
                                connection.query("INSERT INTO `sensors`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "','" + Type + "', '" + Sensor_addr + "', '"+ T_data +"');");
                                connection.query("UPDATE `sensors` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `sensors`.`Address` ='" + Sensor_addr + "';");//обновить дату
                                connection.query("UPDATE `sensors` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `sensors`.`Address` ='" + Sensor_addr + "';");//обновить время
                                console.log("$R: Relay Sensor, active relay № " + Sensor_addr + " create in 'sensors'!");
                                var z;
                                var arr;
                                var h;
                                for (z = 0; z < master_conn_table.length; z++)
                                {
                                    arr = master_conn_table[z];
                                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table[z] = arr;
                                        break;
                                    }
                                }
                                for (z = 0; z < master_conn_table_usb.length; z++)
                                {
                                    arr = master_conn_table_usb[z];
                                    if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table_usb[z] = arr;
                                        break;
                                    }
                                }
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='R'+','+Master_ID+','+Slave_ID+','+Sensor_addr+','+T_data;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'R'+','+Master_ID+','+Slave_ID+','+Sensor_addr+','+T_data+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("$R: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processDO(tempdata)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:
 ********************************************************************/
function processDO(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$DO: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$DO: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                        var Master_ID=tempdata[1];
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var Slave_ID=this[n].Slave_ID;
                                connection.query("DELETE FROM `slave_devices` WHERE  `slave_devices`.`Slave_ID` ='"+ Slave_ID +"';");//удалить слейв с этим ID из таблицы Slave_devices
                                connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_OFF','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "S_OFF" в лог
                                console.log("$DO: Slave device with ID: " + Slave_ID + " offline!... delete from slave_devices!");
                                var z;
                                var arr;
                                var h;
                                for (z = 0; z < master_conn_table.length; z++)
                                {
                                    arr = master_conn_table[z];
                                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table[z] = arr;
                                        break;
                                    }
                                }
                                for (z = 0; z < master_conn_table_usb.length; z++)
                                {
                                    arr = master_conn_table_usb[z];
                                    if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table_usb[z] = arr;
                                        break;
                                    }
                                }
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='DO'+','+Master_ID+','+Slave_ID;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'DO'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0)//совпадений не найдено - обновить лог
                        {
                            var Slave_ID = tempdata[2];
                            connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_OFF','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                            console.log("$DO: Slave device field with ID: " + Slave_ID + " offline, log updated ");
                        }
                        strQuery=("SELECT Slave_ID, Address, Type, Value FROM sensors");//поиск всех датчиков в таблице sensors для нашего слейва
                        check_table(function (){
                            j=0;
                            for (i=0;i<this.length;i++)
                            {
                                if (this[i].Slave_ID==tempdata[2])//найдены датчики у конкретного слейва, удалить их из БД
                                {
                                    j=1;
                                    var Sensor_addr=this[i].Address;
                                    var T_data=this[i].Value;
                                    var Type=this[i].Type;
                                    if (Type=='ds18b20')
                                    {
                                        connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Type +"','"+ Sensor_addr +"','S_OFF','"+ T_data +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "S_OFF" в лог
                                        connection.query("DELETE FROM `sensors` WHERE  `sensors`.`Slave_ID` ='"+ Slave_ID +"';");//удалить из таблицы sensors все датчики этого слейва
                                        console.log("$DO: Sensor " + Sensor_addr + " connected to offline Slave " + Slave_ID + " delete from sensors!");
                                    }
                                    if (Type=='H')
                                    {
                                        connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Type +"','"+ Sensor_addr +"','H_OFF','"+ T_data +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "H_OFF" в лог
                                        connection.query("DELETE FROM `sensors` WHERE  `sensors`.`Slave_ID` ='"+ Slave_ID +"';");//удалить из таблицы sensors все датчики этого слейва
                                        console.log("$DO: Sensor " + Sensor_addr + " connected to offline Slave " + Slave_ID + " delete from sensors!");
                                    }
                                    if (Type=='R')
                                    {
                                        connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Type +"','"+ Sensor_addr +"','R_OFF','"+ T_data +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "H_OFF" в лог
                                        connection.query("DELETE FROM `sensors` WHERE  `sensors`.`Slave_ID` ='"+ Slave_ID +"';");//удалить из таблицы sensors все датчики этого слейва
                                        console.log("$DO: Sensor " + Type + " connected to offline Slave " + Slave_ID + " delete from sensors!");
                                    }

                                }
                            }
                            if (j==0)//совпадений не найдено - просто сообщение в консоль
                            {
                                console.log("$DO: Sensors connected to Slave ID: " + Slave_ID + " not found to delete!");
                            }
                        });
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("$DO: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processSO(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:
 ********************************************************************/
function processSO(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$SO: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        connection.query("INSERT DELAYED INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$SO: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                        var Master_ID=tempdata[1];
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var Slave_ID=this[n].Slave_ID;
                                connection.query("UPDATE `slave_devices` SET `Master_ID` =('"+ Master_ID +"') WHERE  `slave_devices`.`Slave_ID` ='"+ Slave_ID +"';");//обновить MASTER_ID для этого слейва (вдруг поменяли мастера)
                                connection.query("UPDATE `slave_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `slave_devices`.`Master_ID` ='"+ Slave_ID +"';");//обновить дату
                                connection.query("UPDATE `slave_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `slave_devices`.`SLAVE_ID` ='"+ Slave_ID +"';");//обновить время
                                console.log("$SO: Slave device with ID: " + Slave_ID + " already exist in slave_devices!... datum updated!");
                            }
                        }
                        if (j==0)//совпадений не найдено - обновить лог
                        {
                            var Slave_ID = tempdata[2];
                            connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                            connection.query("INSERT INTO `slave_devices`(Customer_ID, Master_ID, Slave_ID) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "');");
                            connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить дату
                            connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить время
                            console.log("$SO: Slave device field with ID: " + Slave_ID + " create in 'slave_devices'!");
                        }
                        strQuery=("SELECT Slave_ID, Address, Type, Value FROM sensors");//поиск всех датчиков в таблице sensors для нашего слейва
                        check_table(function (){
                            j=0;
                            for (n=0;n<this.length;n++)
                            {
                                if (this[n].Address==tempdata[3])//найден  датчик с нужным адресом у конкретного слейва, удалить его из БД, добавить событие в sensors_log
                                {
                                    j=1;
                                    var Sensor_addr=this[n].Address;
                                    var T_data=this[n].Value;
                                    var Type=this[n].Type;
                                    connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Type +"','"+ Sensor_addr +"','T_ERR','"+ T_data +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "T_ERR в лог
                                    connection.query("DELETE FROM `sensors` WHERE  `sensors`.`Address` ='"+ Sensor_addr +"';");//удалить из таблицы sensors датчик с таким адресом
                                    console.log("$SO: Sensor Address " + Sensor_addr + " connected to Slave " + Slave_ID + " delete from sensors!");
                                    var z;
                                    var arr;
                                    var h;
                                    for (z = 0; z < master_conn_table.length; z++)
                                    {
                                        arr = master_conn_table[z];
                                        if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                        {
                                            arr.Master_ID = Master_ID;
                                            master_conn_table[z] = arr;
                                            break;
                                        }
                                    }
                                    for (z = 0; z < master_conn_table_usb.length; z++)
                                    {
                                        arr = master_conn_table_usb[z];
                                        if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                        {
                                            arr.Master_ID = Master_ID;
                                            master_conn_table_usb[z] = arr;
                                            break;
                                        }
                                    }
                                    for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                    {
                                        arr = client_conn_table[h];
                                        if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                        {
                                            streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                                if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                    var data='SO'+','+Master_ID+','+Slave_ID+','+Sensor_addr;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'SO'+','+Master_ID+','+Slave_ID+','+Sensor_addr+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            if (j==0)//совпадений не найдено - просто сообщение в консоль
                            {
                                var Sensor_addr=tempdata[3];
                                console.log("$SO: Sensors connected to Slave ID: " + Slave_ID + " with Address " + Sensor_addr + "  not found to delete!");
                            }
                        });
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("$SO: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processSE(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:
 ********************************************************************/
function processSE(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$SE: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        connection.query("INSERT DELAYED INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$SE: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                        var Master_ID=tempdata[1];
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var Slave_ID=this[n].Slave_ID;
                                connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_ERR','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "T_ERR" в лог
                                connection.query("UPDATE `slave_devices` SET `Master_ID` =('"+ Master_ID +"') WHERE  `slave_devices`.`Slave_ID` ='"+ Slave_ID +"';");//обновить MASTER_ID для этого слейва (вдруг поменяли мастера)
                                connection.query("UPDATE `slave_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `slave_devices`.`Master_ID` ='"+ Slave_ID +"';");//обновить дату
                                connection.query("UPDATE `slave_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `slave_devices`.`SLAVE_ID` ='"+ Slave_ID +"';");//обновить время
                                console.log("$SE: Slave device with ID: " + Slave_ID + " already exist in slave_devices!... datum updated!");
                                var z;
                                var arr;
                                var h;
                                for (z = 0; z < master_conn_table.length; z++)
                                {
                                    arr = master_conn_table[z];
                                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table[z] = arr;
                                        break;
                                    }
                                }
                                for (z = 0; z < master_conn_table_usb.length; z++)
                                {
                                    arr = master_conn_table_usb[z];
                                    if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table_usb[z] = arr;
                                        break;
                                    }
                                }
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='SE'+','+Master_ID+','+Slave_ID;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'SE'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0)//совпадений не найдено - обновить лог
                        {
                            var Slave_ID = tempdata[2];
                            connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Slave_device!" в лог
                            connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_ERR','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "S_ERR" в лог
                            connection.query("INSERT INTO `slave_devices`(Customer_ID, Master_ID, Slave_ID) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "');");
                            connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить дату
                            connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить время
                            console.log("$SE: Slave device field with ID: " + Slave_ID + " create in 'slave_devices'!");
                            var z;
                            var arr;
                            var h;
                            for (z = 0; z < master_conn_table.length; z++)
                            {
                                arr = master_conn_table[z];
                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table[z] = arr;
                                    break;
                                }
                            }
                            for (z = 0; z < master_conn_table_usb.length; z++)
                            {
                                arr = master_conn_table_usb[z];
                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table_usb[z] = arr;
                                    break;
                                }
                            }
                            for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                            {
                                arr = client_conn_table[h];
                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                {
                                    streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='SE'+','+Master_ID+','+Slave_ID;
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'SE'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                        strQuery=("SELECT Slave_ID, Address, Type, Value FROM sensors");//поиск всех датчиков в таблице sensors для нашего слейва
                        check_table(function (){
                            j=0;
                            for (n=0;n<this.length;n++)
                            {
                                if (this[n].Slave_ID==tempdata[2])//найдены датчики у конкретного слейва, удалить их из БД
                                {
                                    j=1;
                                    var Sensor_addr=this[n].Address;
                                    var T_data=this[n].Value;
                                    var Type=this[n].Type;
                                    if (Type=='ds18b20')
                                    {
                                        connection.query("INSERT DELAYED INTO `sensors_log`(Customer_ID, Master_ID, Slave_ID, Type, Address, Event, Value, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','"+ Type +"','"+ Sensor_addr +"','T_ERR','"+ T_data +"','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "T_ERR" в лог
                                        connection.query("DELETE FROM `sensors` WHERE  `sensors`.`Address` ='"+ Sensor_addr +"';");//удалить из таблицы sensors все датчики этого слейва
                                        console.log("$SE: Sensor " + Sensor_addr + " line error or any sensors connected to Slave: " + Slave_ID + ", sensor delete from sensors");
                                    }
                                }
                            }
                            if (j==0)//совпадений не найдено - просто сообщение в консоль
                            {
                                console.log("$SE: Sensors connected to Slave ID: " + Slave_ID + " not found to delete!");
                            }
                        });
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("$SE: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}

/*********************************************************************
 * Function:         processSIM(tempdata)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           tempdata
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:            //ответ мастера с ID
 ********************************************************************/
function processSIM(tempdata, remoteAddress, remotePort)
{

    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var Master_ID
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - то просто обновить дату и время и лог
                        {
                            j=1;
                            Master_ID=this[n].Master_ID;
                            connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                            connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить дату
                            connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ Master_ID +"';");//обновить время
                            console.log("$SIM: Master device with ID: " + Master_ID + " already exist in master_devices!... datum updated!");
                            var z;
                            var arr;
                            for (z = 0; z < master_conn_table.length; z++)
                            {
                                arr = master_conn_table[z];
                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table[z] = arr;
                                    break;
                                }
                            }
                            for (z = 0; z < master_conn_table_usb.length; z++)
                            {
                                arr = master_conn_table_usb[z];
                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table_usb[z] = arr;
                                    break;
                                }
                            }
                            for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                            {
                                arr = client_conn_table[h];
                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                {
                                    streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='CSS'+','+Master_ID+','+'ON';
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'CSS'+','+Master_ID+','+'ON'+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                    }
                    if (j==0)//совпадений не найдено - вставить поле с новым Master_ID в таблицу master_devices, вставить дату и время, записать событие в лог
                    {
                        Master_ID=tempdata[1];
                        for (z = 0; z < master_conn_table.length; z++)
                        {
                            arr = master_conn_table[z];
                            if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                            {
                                arr.Master_ID = Master_ID;
                                master_conn_table[z] = arr;
                                break;
                            }
                        }
                        for (z = 0; z < master_conn_table_usb.length; z++)
                        {
                            arr = master_conn_table_usb[z];
                            if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                            {
                                arr.Master_ID = Master_ID;
                                master_conn_table_usb[z] = arr;
                                break;
                            }
                        }
                        for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                            {
                                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                        var data='CSS'+','+Master_ID+','+'ON';
                                        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CSS'+','+Master_ID+','+'ON'+','+calc_checksum+'\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');
                                    }
                                });
                            }
                        }
                        connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"','M_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        connection.query("INSERT INTO `master_devices`(Customer_ID, Master_ID) VALUES ('"+ Customer_ID +"','"+ tempdata[1] +"');");//создать поле с новым Master_ID
                        connection.query("UPDATE `master_devices` SET `Date` =('"+ year +"' '-' '"+ month +"' '-' '"+ date +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить дату
                        connection.query("UPDATE `master_devices` SET `Time` =('"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"') WHERE  `master_devices`.`Master_ID` ='"+ tempdata[1] +"';");//обновить время
                        console.log("$SIM: Master device field with ID: " + tempdata[1] + " create in master_devices'!");
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("$SIM: SEMaster device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processSLC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:            get slave_device log
 ********************************************************************/
function processSLC(tempdata, remoteAddress, remotePort)
{
    var talker1="CLS";//ответ сервера "строка событий"
    var talker2="CVS";//ответ сервера "строка напряжения"
    var temp_date_log={};
    temp_date_log.year;
    temp_date_log.month;
    temp_date_log.day;
    var temp_time_log={};
    temp_time_log.hour;
    temp_time_log.minute;
    temp_date_log.second;
    var temp_date_first_input={};
    temp_date_first_input.year;
    temp_date_first_input.month;
    temp_date_first_input.day;
    var temp_date_last_input={};
    temp_date_last_input.year;
    temp_date_last_input.month;
    temp_date_last_input.day;
    var temp_time_first_input={};
    temp_time_first_input.hour;
    temp_time_first_input.minute;
    temp_time_first_input.second;
    var temp_time_last_input={};
    temp_time_last_input.hour;
    temp_time_last_input.minute;
    temp_time_last_input.second;
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Customer_ID, Master_ID, Slave_ID, Event, Battery_V, Date, Time FROM slave_log");//проверить slave_log на наличие записей этого слейва
                check_table(function () {
                    var j=0;
                    for (i=0;i<this.length;i++)
                    {
                        this[i].Date=this[i].Date.toLocaleDateString(); //преобразовать формат даты MySQL
                        if (this[i].Slave_ID==tempdata[2])//записи существуют?
                        {
                            var Master_ID=this[i].Master_ID;
                            var Slave_ID=this[i].Slave_ID;
                            var Event=this[i].Event;
                            var Battery_V=this[i].Battery_V;
                            var Date=this[i].Date;
                            var Time=this[i].Time;
                            temp_date_log=this[i].Date.split('-');
                            temp_time_log=this[i].Time.split(':');
                            if (check_log_datum(temp_date_log, temp_time_log, tempdata)==true)// если запись лога соответствует временном запросу - формировать строку на отправку
                            {
                                j=1;
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                if (Event!="0")
                                                {
                                                    var data='CLS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Event;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'CLS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Event+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                                else
                                                {
                                                    var data='$'+'CVS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Battery_V;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'CVS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Battery_V+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        console.log("$SLC: Request error: " + this[i].Master_ID + " not registered!");
                                    }
                                }//формировать строку согласно протоколу
                            }
                            console.log("$SLC: Slave device log with ID: " + this[i].Slave_ID + " found in slave_log!");
                        }
                    }
                    if (j==0) // этот слейв отсутствует либо запрос лога некорректен по дате, либо записи отсутствуют
                    {
                        for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                            {
                                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {

                                        var data='CLS'+','+Master_ID+','+Slave_ID+','+""+','+""+','+"NODATA";
                                        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CLS'+','+Master_ID+','+Slave_ID+','+""+','+""+','+"NODATA"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');

                                        var data='CVS'+','+Master_ID+','+Slave_ID+','+""+','+""+','+"NODATA";
                                        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CVS'+','+Master_ID+','+Slave_ID+','+""+','+""+','+"NODATA"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');
                                    }
                                });
                            }
                            else
                            {
                                console.log("$SLC: Request error: " + tempdata[1] + " not registered!");
                            }
                        }//формировать строку согласно протоколу
                        console.log("$SLC: Request log for Slave device log error: ID: " + tempdata[2] + " no data in database!");
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("$SLC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}

/*********************************************************************
 * Function:         processSTC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:            get one temp sensor 18b20 log
 ********************************************************************/
function processSTC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Customer_ID, Master_ID, Slave_ID, Event, Address, Value, Type, Date, Time FROM sensors_log");//проверить sensors_log на наличие записей этого датчика
                check_table(function () {
                    var j=0;
                    for (i=0;i<this.length;i++)
                    {
                        this[i].Date=this[i].Date.toLocaleDateString(); //преобразовать формат даты MySQL
                        var Master_ID=this[i].Master_ID;
                        if (this[i].Address==tempdata[7])//записи существуют?
                        {
                            var Slave_ID=this[i].Slave_ID;
                            var Event=this[i].Event;
                            var Value=this[i].Value;
                            var Address=this[i].Address;
                            var Type=this[i].Type;
                            var Date=this[i].Date;
                            var Time=this[i].Time;
                            temp_date_log=this[i].Date.split('-');
                            temp_time_log=this[i].Time.split(':');
                            if (check_log_datum(temp_date_log, temp_time_log, tempdata)==true)// если запись лога соответствует временном запросу - формировать строку на отправку
                            {
                                j=1;
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                if (Event!="0" && Type=="ds18b20")
                                                {
                                                    var data='CES'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Address+','+Event;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'CES'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Address+','+Value+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                                if (Event=="0" && Type=="ds18b20")
                                                {
                                                    var data = '$' + 'CTS' + ',' + Master_ID + ',' + Slave_ID + ',' + Date + ',' + Time + ',' + Address+','+Value;
                                                    var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data = '$' + 'CTS' + ',' + Master_ID + ',' + Slave_ID + ',' + Date + ',' + Time + ',' + Address +','+ Value + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        console.log("STC: Request (get T log) error: " + this[i].Master_ID + " not registered!");
                                    }
                                }
                            }
                        }
                    }
                    if (j==0)
                    {
                        for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                            {
                                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                        var data='CES'+','+Master_ID+','+tempdata[2]+','+""+','+""+','+tempdata[7]+','+"NODATA";
                                        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CES'+','+Master_ID+','+tempdata[2]+','+""+','+""+','+tempdata[7]+','+"NODATA"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');

                                        var data='CTS'+','+Master_ID+','+tempdata[2]+','+""+','+""+','+tempdata[7]+','+"NODATA";
                                        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CTS'+','+Master_ID+','+tempdata[2]+','+""+','+""+','+tempdata[7]+','+"NODATA"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');
                                    }
                                });
                            }
                            else
                            {
                                console.log("$STC: Request error: Master_ID " + tempdata[1] + " not registered!");
                            }
                        }//формировать строку согласно протоколу
                        console.log("$STC: Request log for ds18b20 error: " + tempdata[7] + " no data in database!");
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("$STC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processSHC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:            get one Hudmunity sensor log
 ********************************************************************/
function processSHC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Customer_ID, Master_ID, Slave_ID, Event, Address, Value, Type, Date, Time FROM sensors_log");//проверить sensors_log на наличие записей этого датчика
                check_table(function () {
                    var j=0;
                    var Master_ID=this[i].Master_ID;
                    for (i=0;i<this.length;i++)
                    {
                        this[i].Date=this[i].Date.toLocaleDateString(); //преобразовать формат даты MySQL
                        if (this[i].Address=="H")//записи существуют?
                        {
                            var Slave_ID=this[i].Slave_ID;
                            var Event=this[i].Event;
                            var Value=this[i].Value;
                            var Address=this[i].Address;
                            var Type=this[i].Type;
                            var Date=this[i].Date;
                            var Time=this[i].Time;
                            temp_date_log=this[i].Date.split('-');
                            temp_time_log=this[i].Time.split(':');
                            if (check_log_datum(temp_date_log, temp_time_log, tempdata)==true)// если запись лога соответствует временном запросу - формировать строку на отправку
                            {
                                j=1;
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                if (Event!="0" && Type=="H")//отправить строку данных
                                                {
                                                    var data='CBS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Event;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'CBS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Event+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                                if (Event=="0" && Type=="H")//отправить строку событий
                                                {
                                                    var data = '$' + 'CHS' + ',' + Master_ID + ',' + Slave_ID + ',' + Date + ',' + Time + ',' +Value;
                                                    var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data = '$' + 'CHS' + ',' + Master_ID + ',' + Slave_ID + ',' + Date + ',' + Time + ',' + Value + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        console.log("$SHC: Request (get H log) error: " + this[i].Master_ID + " not registered!");
                                    }
                                }
                            }
                        }
                    }
                    if (j==0) {
                        for (h = 0; h < client_conn_table.length; h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес в клиентской таблице подключений?
                            {
                                streams.forEach(function (stream) {           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                    if (arr.IP == stream.remoteAddress && arr.Port == stream.remotePort) {
                                        var data = 'CBS' + ',' + tempdata[1] + ',' + tempdata[2] + ',' + "" + ',' + "" + ',' + "NODATA";
                                        var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data = '$' + 'CBS' + ',' + tempdata[1] + ',' + tempdata[2] + ',' + "" + ',' + "" + ',' + "NODATA" + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');

                                        var data = '$' + 'CHS' + ',' + tempdata[1] + ',' + tempdata[2] + ',' + "" + ',' + "" + ',' + "NODATA";
                                        var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data = '$' + 'CHS' + ',' + tempdata[1] + ',' + tempdata[2] + ',' + "" + ',' + "" + ',' + "NODATA" + ',' + calc_checksum + '\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');
                                    }
                                });
                            }
                            console.log("$SHS: Request log for Hudmunity sensor error, Slave_ID: " + tempdata[2] + " no data in database!");
                        }
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("$SHS: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processSRC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:            get Relay sensor log
 ********************************************************************/
function processSRC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Customer_ID, Master_ID, Slave_ID, Event, Address, Value, Type, Date, Time FROM sensors_log");//проверить sensors_log на наличие записей этого датчика
                check_table(function () {
                    var j=0;
                    var Master_ID=this[i].Master_ID;
                    for (i=0;i<this.length;i++)
                    {
                        this[i].Date=this[i].Date.toLocaleDateString(); //преобразовать формат даты MySQL
                        if (this[i].Type=="R" && this[i].Slave_ID==tempdata[2])//записи существуют?
                        {
                            var Slave_ID=this[i].Slave_ID;
                            var Event=this[i].Event;
                            var Value=this[i].Value;
                            var Address=this[i].Address;
                            var Type=this[i].Type;
                            var Date=this[i].Date;
                            var Time=this[i].Time;
                            temp_date_log=this[i].Date.split('-');
                            temp_time_log=this[i].Time.split(':');
                            if (check_log_datum(temp_date_log, temp_time_log, tempdata)==true)// если запись лога соответствует временному запросу - формировать строку на отправку
                            {
                                j=1;
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                if (Event!="0" && Type=="R")//отправить строку данных
                                                {
                                                    var data='CRS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Address+','+Event;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'CRS'+','+Master_ID+','+Slave_ID+','+Date+','+Time+','+Event+','+Address+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        console.log("SRC: Request (get R log) error. Master_ID " + this[i].Master_ID + " not registered!");
                                    }
                                }
                            }
                        }
                    }
                    if (j==0)
                    {
                        for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес в клиентской таблице подключений?
                            {
                                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                        var data='CRS'+','+Master_ID+','+tempdata[2]+','+""+','+""+','+""+','+"NODATA";
                                        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CRS'+','+Master_ID+','+tempdata[2]+','+""+','+""+','+""+','+"NODATA"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                        stream.write(data, 'utf8');
                                    }
                                });
                            }
                            else
                            {
                                console.log("SRC: Request (get R log) error. Master_ID " + Master_ID + " not registered!");
                            }
                        }
                        console.log("SRC: Request log for Relay sensor error. Slave_ID: " + tempdata[2] + " no data in database!");
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("SRC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processSMC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function write data to MYSQL with date and time
 *
 * Note:            get Master device log
 ********************************************************************/
function processSMC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        // var bla=tempdata;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Customer_ID, Master_ID, Event, Date, Time FROM master_log");//проверить master_log на наличие записей
                check_table(function () {
                    var j=0;
                    var Master_ID=this[i].Master_ID;
                    for (i=0;i<this.length;i++)
                    {
                        this[i].Date=this[i].Date.toLocaleDateString(); //преобразовать формат даты MySQL
                        if (this[i].Master_ID==tempdata[1])//записи существуют?
                        {
                            var Event=this[i].Event;
                            var Date=this[i].Date;
                            var Time=this[i].Time;
                            temp_date_log=this[i].Date.split('-');
                            temp_time_log=this[i].Time.split(':');
                            if (check_log_datum(temp_date_log, temp_time_log, tempdata)==true)// если запись лога соответствует временном запросу - формировать строку на отправку
                            {
                                j=1;
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                //отправить строку данных
                                                var data ='CMS' + ',' + Master_ID +','+ Date +','+ Time +','+ Event;
                                                var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'CMS'+','+Master_ID+','+Date+','+Time+','+Event+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data,'utf8');
                                            }
                                        });
                                    }
                                    else
                                    {
                                        console.log("$SMS: Request (get Master log) error. Master_ID " + this[i].Master_ID + " not registered!");
                                    }
                                }
                            }
                        }
                    }
                    if (j==0)
                    {
                        for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес в клиентской таблице подключений?
                            {
                                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                        //отправить строку данных
                                        var data ='CMS' + ',' + Master_ID +','+ "" +','+ "" +','+ "NODATA";
                                        var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CMS'+','+Master_ID+','+""+','+""+','+"NODATA"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                        stream.write(data,'utf8');
                                    }
                                });
                            }
                            else
                            {
                                console.log("$SMS: Request (get Master log) error. Master_ID " + Master_ID + " not registered!");
                            }
                        }
                        console.log("$SMS: Request log for Master device error. Master_ID: " + tempdata[1] + " no data in database!");
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("$SMS: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}
/*********************************************************************
 * Function:         processSIC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     tempdata - parsed input tcp checked data
 *
 * Input:            Array
 * Return^           true of false
 *
 * Overview:         This function compare data in MYSQL with tcp ID
 *
 * Note:            check client ID
 ********************************************************************/
function processSIC(tempdata, remoteAddress, remotePort)
{
    var talker1="CIS";//ответ сервера "подтверждение ID клиента"
    var data;
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        var j=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].Customer_ID==tempdata[1])//если совпадение найдено - вернуть 'Y' - такой аккаунт зарегистрирован
            {
                j++;
                k=1;
                var Customer_ID=this[i].Customer_ID;
                var z;
                var arr;
                for (z = 0; z < client_conn_table.length; z++)
                {
                    arr = client_conn_table[z];
                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                    {
                        arr.Customer_ID = this[i].Customer_ID;
                        arr.Master_ID_1 = this[i].MASTER_ID_1;
                        arr.Master_ID_2 = this[i].MASTER_ID_2;
                        arr.Master_ID_3 = this[i].MASTER_ID_3;
                        arr.Master_ID_4 = this[i].MASTER_ID_4;
                        arr.Master_ID_5 = this[i].MASTER_ID_5;
                        client_conn_table[z] = arr;
                        break;
                    }
                }
            }
            if (j>1)
            {
                console.log("Customer_ID with ID: " + Customer_ID + " multiply of Customer_ID in accaunts table!");
            }
        }
        if (k==0)
        {
            console.log("Customer_ID with ID: " + tempdata[1] + " not registered in accaunts table!");
            data='$'+'CIS'+','+'N';//отправить ID не подтверждён!
            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
            data='$'+'CIS'+','+'N'+','+calc_checksum+'\r\n';//формировать строку на отправку
            streams.forEach(function(stream){           //найти кому отправить (сравнить IP в потоке и IP с таблице подключений)
                if (remoteAddress==stream.remoteAddress && remotePort==stream.remotePort) {
                    stream.write(data, 'utf8');
                }
            });
            return false;
        }
        console.log("Customer_ID with ID: " + Customer_ID + " registration succsessful in accaunts table!");
        data='CIS'+','+'Y';//отправить ID подтверждён!
        var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
        data='$'+'CIS'+','+'Y'+','+calc_checksum+'\r\n';//формировать строку на отправку
        streams.forEach(function(stream){           //найти кому отправить (сравнить IP в потоке и IP с таблице подключений)
            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                stream.write(data, 'utf8');
            }
        });
        // onStreamData(arr);
        return true;
    });
}


/*********************************************************************
 * Function:         processSDC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     client connected
 *
 * Input:            save time for client log
 * Return^           none
 *
 * Overview:         This function set log timelife for MySQL
 *
 * Note:             Задать время хранения лога (для всего дерева устройств  этого Customer_ID)
 ********************************************************************/
function processSDC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//существует ли такой Customer_ID в таблице accaunts?
    // connection.query("SET GLOBAL event_scheduler = ON");//включить обработчик событий
    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var Master_ID
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].Customer_ID==tempdata[1])//если совпадение найдено - задать время хранения лога
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SHOW EVENTS");
                check_table(function () {
                    var events=this;
                    var a=0,b=0,c=0;
                    for (n=0;n<events.length;n++)
                    {
                        if(events[n].Name=="loglife_master")//если событие loglife_master существует - изменить его
                        {
                            a=1;
                            connection.query("ALTER EVENT loglife_master ON SCHEDULE EVERY 1 DAY DO DELETE FROM master_log WHERE master_log.Customer_ID="+Customer_ID+" AND master_log.Date <= DATE_SUB(NOW(), INTERVAL "+tempdata[2]+" MONTH) ;");//задать хранение лога master_log
                        }
                        if(events[n].Name=="loglife_slave")//если событие loglife_master существует - изменить его
                        {
                            b=1;
                            connection.query("ALTER EVENT loglife_slave ON SCHEDULE EVERY 1 DAY DO DELETE FROM slave_log WHERE slave_log.Customer_ID="+Customer_ID+" AND slave_log.Date <= DATE_SUB(NOW(), INTERVAL "+tempdata[2]+" MONTH) ;");//задать хранение лога slave_log
                        }
                        if(events[n].Name=="loglife_sensors")//если событие loglife_master существует - изменить его
                        {
                            c=1;
                            connection.query("ALTER EVENT loglife_sensors ON SCHEDULE EVERY 1 DAY DO DELETE FROM sensors_log WHERE sensors_log.Customer_ID="+Customer_ID+" AND sensors_log.Date <= DATE_SUB(NOW(), INTERVAL "+tempdata[2]+" MONTH) ;");//задать хранение лога sensors
                        }
                    }
                    if (a==0 && b==0 && c==0)
                    {
                        for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же Customer_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Customer_ID == Customer_ID)//если есть такой Customer_ID в клиентской таблице подключений?
                            {
                                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                        //отправить строку данных
                                        var data ='CDS' + ',' + 'Y';
                                        var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CDS'+','+'Y'+','+calc_checksum+'\r\n';//формировать строку на отправку (успешно установлено время хранения лога)
                                        stream.write(data,'utf8');
                                    }
                                });
                                connection.query("CREATE EVENT loglife_master ON SCHEDULE EVERY 1 DAY DO DELETE FROM master_log WHERE master_log.Customer_ID="+Customer_ID+" AND master_log.Date <= DATE_SUB(NOW(), INTERVAL "+tempdata[2]+" MONTH) ;");//задать хранение лога master_log
                                connection.query("CREATE EVENT loglife_slave ON SCHEDULE EVERY 1 DAY DO DELETE FROM slave_log WHERE slave_log.Customer_ID="+Customer_ID+" AND slave_log.Date <= DATE_SUB(NOW(), INTERVAL "+tempdata[2]+" MONTH) ;");//задать хранение лога slave_log
                                connection.query("CREATE EVENT loglife_sensors ON SCHEDULE EVERY 1 DAY DO DELETE FROM sensors_log WHERE sensors_log.Customer_ID="+Customer_ID+" AND sensors_log.Date <= DATE_SUB(NOW(), INTERVAL "+tempdata[2]+" MONTH) ;");//задать хранение лога sensors
                                console.log("Customer_ID: " + Customer_ID + " log lifetime create for: " +tempdata[2]+ " Month");
                            }
                        }
                    }
                    else
                    {
                        console.log("Customer_ID: " + Customer_ID + " log lifetime set for: " +tempdata[2]+ " Month");
                        for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же Customer_ID в таблице подключений клиента
                        {
                            arr = client_conn_table[h];
                            if (arr.Customer_ID == Customer_ID)//если есть такой Customer_ID в клиентской таблице подключений?
                            {
                                streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблицы подключений клиентов)
                                    if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                        //отправить строку данных
                                        var data ='CDS' + ',' + 'Y';
                                        var calc_checksum = calc_xor(data).toString(16);//вычислять контрольную сумму
                                        data='$'+'CDS'+','+'Y'+','+calc_checksum+'\r\n';//формировать строку на отправку (успешно установлено время хранения лога)
                                        stream.write(data,'utf8');
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }
        if (k==0 && tempdata[1]!=undefined)
        {
            console.log("Customer_ID: " + tempdata[1] + " (log lifetime set) not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         add_to_master_log(Master_ID_discon)
 *
 * PreCondition:     master off or disconnected
 *
 * Input:            Master_ID_discon
 * Return^           none
 *
 * Overview:         This function write event M_Off to master_log tAble
 *
 * Note:            check Customer_ID
 ********************************************************************/
function add_to_master_log(Master_ID_discon)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var Master_ID
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==Master_ID_discon || this[i].MASTER_ID_2==Master_ID_discon || this[i].MASTER_ID_3==Master_ID_discon || this[i].MASTER_ID_4==Master_ID_discon || this[i].MASTER_ID_5==Master_ID_discon)//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (i=0;i<this.length;i++)
                    {
                        if (this[i].Master_ID==Master_ID_discon)//Если да - удалить его
                        {
                            j=1;
                            if (Master_ID_discon!=undefined)//событие "мастер отключился"
                            {
                                connection.query("DELETE FROM `master_devices` WHERE  `master_devices`.`Master_ID` ='"+ Master_ID_discon +"';");//удалить слейв с этим ID из таблицы Slave_devices
                                connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('" + Customer_ID + "','" + Master_ID_discon + "','M_OFF','" + year + "-" + month + "-" + date + "','" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "');");//записать событие "обнаружен новый Master_device!" в лог
                                console.log("Master device with ID: " + Master_ID_discon + " disconnected, delete from master_devices, master log updated!");
                            }
                        }
                    }
                    if (j==0)//совпадений не найдено - записать событие в лог
                    {
                        connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID_discon +"','M_OFF','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                        console.log("Master device with ID: " + Master_ID_discon + " disconnected, not issue in master_devices, master_log updated!");
                    }
                });
            }
        }
        if (k==0 && Master_ID_discon!=undefined)
        {
            connection.query("INSERT INTO `master_log`(Customer_ID, Master_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID_discon +"','M_OFF','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
            console.log("Master device with ID: " + Master_ID_discon + " disconnected, not registered in accaunts table, master_log updated!");
        }
    });
}


/*********************************************************************
 * Function:         processMRC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:            Command fo relay device from tcp client
 * Return^           none
 *
 * Overview:         This function search device with same ID and translate command
 *
 * Note:             Установить выход реле (tcp Клиент-Мастеру)
 ********************************************************************/
function processMRC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("MRC: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: MRC: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                        var Master_ID=tempdata[1];
                        return;
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var Slave_ID=this[n].Slave_ID;
                            }
                        }
                        if (j==0)////совпадений не найдено - вывести ошибку
                        {
                            var Slave_ID = tempdata[2];
                            console.log("MRC: Slave device with ID: " + Slave_ID + " not found in slave_devices'!");
                            intel.error('WARNING: MRC: Slave device with ID: ' + Slave_ID +' not found in slave_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                            return;
                        }
                        strQuery=("SELECT Type FROM sensors");//поиск всех реле в таблице sensors
                        check_table(function (){
                            j=0;
                            for (n=0;n<this.length;n++)
                            {
                                if (this[n].Type=="R" && Slave_ID==tempdata[2])//найден блок реле у конкретного слейва, переслать данные мастеру
                                {
                                    j=1;
                                    var Relay_number=tempdata[3];//номер реле
                                    var Relay_state=tempdata[4];
                                    var Type="R";
                                    var z;
                                    var arr;
                                    var h;

                                    for(h=0;h<master_conn_table.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (tcp)
                                    {
                                        arr = master_conn_table[h];
                                        if (arr.Master_ID == Master_ID)//есть мастер с ID запроса в таблице подключений tcp?
                                        {
                                            streams.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                                if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                    var data='MRC'+','+Master_ID+','+Slave_ID+','+Relay_number+','+Relay_state;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'MRC'+','+Master_ID+','+Slave_ID+','+Relay_number+','+Relay_state+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    stream.write(data, 'utf8');
                                                }
                                            });
                                        }
                                    }

                                    for(h=0;h<master_conn_table_usb.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (usb)
                                    {
                                        arr = master_conn_table_usb[h];
                                        if (arr.Master_ID == Master_ID)//есть мастер с Master_ID в запросе в таблице подключений usb? - переслать данные мастеру
                                        {
                                            streams_usb.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                                if (arr.busNumber==stream.busNumber && arr.deviceAddress==stream.deviceAddress) {
                                                    var endpoints = stream.interfaces[1].endpoints,// интерфейс 1 - интерфейс данных
                                                        inEndpoint = endpoints[1], // конечная точка 1 - входящие данные
                                                        outEndpoint = endpoints[0];// конечная точка 0 - исходящие данные
                                                    var data='MRC'+','+Master_ID+','+Slave_ID+','+Relay_number+','+Relay_state;
                                                    var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                    data='$'+'MRC'+','+Master_ID+','+Slave_ID+','+Relay_number+','+Relay_state+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                    outEndpoint.transfer(data, 'utf8');
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            if (j==0)//совпадений не найдено - вставить поле с новым type=R в таблицу sensors, вставить дату и время, добавить в лог sensors_log событие S_ON
                            {
                                var Relay_number=tempdata[3];//номер реле
                                var Relay_state=tempdata[4];
                                var Type="R";
                                var z;
                                var arr;
                                var h;
                            }
                        });
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("MRC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processMSC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:            Command fo relay device from tcp client
 * Return^           none
 *
 * Overview:         This function search device with same ID and translate command
 *
 * Note:            Установить время сна (tcp Клиент-Мастеру)
 ********************************************************************/
function processMSC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("MSC: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: MSC: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                        var Master_ID=tempdata[1];
                        return;
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var Slave_ID=this[n].Slave_ID;
                                var Sleep_time=tempdata[3];
                                var z;
                                var arr;
                                var h;
                                for(h=0;h<master_conn_table.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (tcp)
                                {
                                    arr = master_conn_table[h];
                                    if (arr.Master_ID == Master_ID)//есть мастер с ID запроса в таблице подключений tcp?
                                    {
                                        streams.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='MSC'+','+Master_ID+','+Slave_ID+','+Sleep_time;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'MSC'+','+Master_ID+','+Slave_ID+','+Sleep_time+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                                for(h=0;h<master_conn_table_usb.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (usb)
                                {
                                    arr = master_conn_table_usb[h];
                                    if (arr.Master_ID == Master_ID)//есть мастер с Master_ID в запросе в таблице подключений usb? - переслать данные мастеру
                                    {
                                        streams_usb.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                            if (arr.busNumber==stream.busNumber && arr.deviceAddress==stream.deviceAddress) {
                                                var endpoints = stream.interfaces[1].endpoints,// интерфейс 1 - интерфейс данных
                                                    inEndpoint = endpoints[1], // конечная точка 1 - входящие данные
                                                    outEndpoint = endpoints[0];// конечная точка 0 - исходящие данные
                                                var data='MSC'+','+Master_ID+','+Slave_ID+','+Sleep_time;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'MSC'+','+Master_ID+','+Slave_ID+','+Sleep_time+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                outEndpoint.transfer(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0)////совпадений не найдено - вывести ошибку
                        {
                            var Slave_ID = tempdata[2];
                            console.log("MCS: Slave device with ID: " + Slave_ID + " not found in slave_devices'!");
                            intel.error('WARNING: MCS: Slave device with ID: ' + Slave_ID +' not found in slave_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                        }
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("MRC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processMTC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:
 * Return^           none
 *
 * Overview:
 *
 * Note:            Запрос мастеру: Получить сохраненный список слейвов
 ********************************************************************/
function processMTC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    var n=0;
                    var Master_ID=this[n].Master_ID;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            j=1;
                            Master_ID=this[n].Master_ID;
                            var z;
                            var arr;
                            var h;
                            for(h=0;h<master_conn_table.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (tcp)
                            {
                                arr = master_conn_table[h];
                                if (arr.Master_ID == Master_ID)//есть мастер с ID запроса в таблице подключений tcp?
                                {
                                    streams.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='MTC'+','+Master_ID;
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'MTC'+','+Master_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                            for(h=0;h<master_conn_table_usb.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (usb)
                            {
                                arr = master_conn_table_usb[h];
                                if (arr.Master_ID == Master_ID)//есть мастер с Master_ID в запросе в таблице подключений usb? - переслать данные мастеру
                                {
                                    streams_usb.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                        if (arr.busNumber==stream.busNumber && arr.deviceAddress==stream.deviceAddress) {
                                            var endpoints = stream.interfaces[1].endpoints,// интерфейс 1 - интерфейс данных
                                                inEndpoint = endpoints[1], // конечная точка 1 - входящие данные
                                                outEndpoint = endpoints[0];// конечная точка 0 - исходящие данные
                                            var data='MTC'+','+Master_ID;
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'MTC'+','+Master_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            outEndpoint.transfer(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("MTC: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: MTC: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("MTC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}

/*********************************************************************
 * Function:         processCTM(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:
 * Return^           none
 *
 * Overview:
 *
 * Note:            Ответ мастера: адрес слейва (один слейв - одно сообщение)
 ********************************************************************/
function processCTM(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        var n=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("CTM: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: CTM: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                        var Master_ID=tempdata[1];
                        return;
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        var Slave_ID=tempdata[2];
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                Slave_ID=this[n].Slave_ID;
                                j=1;
                                var z;
                                var arr;
                                var h;
                                for (z = 0; z < master_conn_table.length; z++)
                                {
                                    arr = master_conn_table[z];
                                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table[z] = arr;
                                        break;
                                    }
                                }
                                for (z = 0; z < master_conn_table_usb.length; z++)
                                {
                                    arr = master_conn_table_usb[z];
                                    if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table_usb[z] = arr;
                                        break;
                                    }
                                }
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='CTM'+','+Master_ID+','+Slave_ID;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'CTM'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0)////совпадений не найдено - вывести ошибку
                        {
                            var Slave_ID = tempdata[2];
                            console.log("CTM: Slave device with ID: " + Slave_ID + " not found in slave_devices'!");
                            intel.error('WARNING: CTM: Slave device with ID: ' + Slave_ID +' not found in slave_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                            connection.query("INSERT DELAYED INTO `slave_log`(Customer_ID, Master_ID, Slave_ID, Event, Date, Time) VALUES ('"+ Customer_ID +"','"+ Master_ID +"','"+ Slave_ID +"','S_ON','"+ year +"-"+ month +"-"+ date +"','"+ time.hours +"' ':' '"+ time.minutes +"' ':' '"+ time.seconds +"');");//записать событие "обнаружен новый Master_device!" в лог
                            connection.query("INSERT INTO `slave_devices`(Customer_ID, Master_ID, Slave_ID) VALUES ('" + Customer_ID + "','" + Master_ID + "','" + Slave_ID + "');");
                            connection.query("UPDATE `slave_devices` SET `Date` =('" + year + "' '-' '" + month + "' '-' '" + date + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить дату
                            connection.query("UPDATE `slave_devices` SET `Time` =('" + time.hours + "' ':' '" + time.minutes + "' ':' '" + time.seconds + "') WHERE  `slave_devices`.`Slave_ID` ='" + Slave_ID + "';");//обновить время
                            console.log("$CTM: Slave device field with ID: " + Slave_ID + " create in 'slave_devices'!");
                            var z;
                            var arr;
                            var h;
                            for (z = 0; z < master_conn_table.length; z++)
                            {
                                arr = master_conn_table[z];
                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table[z] = arr;
                                    break;
                                }
                            }
                            for (z = 0; z < master_conn_table_usb.length; z++)
                            {
                                arr = master_conn_table_usb[z];
                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table_usb[z] = arr;
                                    break;
                                }
                            }
                            for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                            {
                                arr = client_conn_table[h];
                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                {
                                    streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='CTM'+','+Master_ID+','+Slave_ID;
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'CTM'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("CTM: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}

/*********************************************************************
 * Function:         processMFC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:
 * Return^           none
 *
 * Overview:
 *
 * Note:            Запрос мастеру: Включить поиск новых слейвов
 ********************************************************************/
function processMFC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        var n=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    var Master_ID=tempdata[1];
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            Master_ID=this[n].Master_ID;
                            j=1;
                            var z;
                            var arr;
                            var h;
                            for(h=0;h<master_conn_table.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (tcp)
                            {
                                arr = master_conn_table[h];
                                if (arr.Master_ID == Master_ID)//есть мастер с ID запроса в таблице подключений tcp?
                                {
                                    streams.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='MFC'+','+Master_ID;
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'MFC'+','+Master_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                            for(h=0;h<master_conn_table_usb.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (usb)
                            {
                                arr = master_conn_table_usb[h];
                                if (arr.Master_ID == Master_ID)//есть мастер с Master_ID в запросе в таблице подключений usb? - переслать данные мастеру
                                {
                                    streams_usb.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                        if (arr.busNumber==stream.busNumber && arr.deviceAddress==stream.deviceAddress) {
                                            var endpoints = stream.interfaces[1].endpoints,// интерфейс 1 - интерфейс данных
                                                inEndpoint = endpoints[1], // конечная точка 1 - входящие данные
                                                outEndpoint = endpoints[0];// конечная точка 0 - исходящие данные
                                            var data='MFC'+','+Master_ID;
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'MFC'+','+Master_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            outEndpoint.transfer(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("MFC: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: MFC: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                    }
                });
            }
        }
        if (k==0)
        {
            console.log("MFC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processCFM(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:
 * Return^           none
 *
 * Overview:
 *
 * Note:            Ответ мастера: Добавлен новый слейв (один слейв - одно сообщение)
 ********************************************************************/
function processCFM(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        var n=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("CFM: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: CFM: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                        var Master_ID=tempdata[1];
                        return;
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                var Slave_ID=this[n].Slave_ID;
                                j=1;
                                var z;
                                var arr;
                                var h;
                                for (z = 0; z < master_conn_table.length; z++)
                                {
                                    arr = master_conn_table[z];
                                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table[z] = arr;
                                        break;
                                    }
                                }
                                for (z = 0; z < master_conn_table_usb.length; z++)
                                {
                                    arr = master_conn_table_usb[z];
                                    if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table_usb[z] = arr;
                                        break;
                                    }
                                }
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='CFM'+','+Master_ID+','+Slave_ID;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'CFM'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0 && tempdata[2]=="OFF")//Ответ мастера по окончанию поиска: Поиск завершен
                        {
                            intel.error('WARNING: CFM: Slave device with ID: ' + tempdata[2] +' not found in slave_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                            var z;
                            var arr;
                            var h;
                            for (z = 0; z < master_conn_table.length; z++)
                            {
                                arr = master_conn_table[z];
                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table[z] = arr;
                                    break;
                                }
                            }
                            for (z = 0; z < master_conn_table_usb.length; z++)
                            {
                                arr = master_conn_table_usb[z];
                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table_usb[z] = arr;
                                    break;
                                }
                            }
                            for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                            {
                                arr = client_conn_table[h];
                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                {
                                    streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='CFM'+','+Master_ID+','+"OFF";
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'CFM'+','+Master_ID+','+"OFF"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                        if (j==0 && tempdata[2]=="OVR")//Ответ мастера: Таблица переполнена
                        {
                            intel.error('WARNING: CFM: Slave device with ID: ' + tempdata[2] +' not found in slave_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                            var z;
                            var arr;
                            var h;
                            for (z = 0; z < master_conn_table.length; z++)
                            {
                                arr = master_conn_table[z];
                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table[z] = arr;
                                    break;
                                }
                            }
                            for (z = 0; z < master_conn_table_usb.length; z++)
                            {
                                arr = master_conn_table_usb[z];
                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table_usb[z] = arr;
                                    break;
                                }
                            }
                            for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                            {
                                arr = client_conn_table[h];
                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                {
                                    streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='CFM'+','+Master_ID+','+"OVR";
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'CFM'+','+Master_ID+','+"OVR"+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }

                    });
                });
            }
        }
        if (k==0)
        {
            console.log("CFM: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processMDC(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:
 * Return^           none
 *
 * Overview:
 *
 * Note:            Запрос мастеру: Удалить слейв (один слейв - одно сообщение)
 ********************************************************************/
function processMDC(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        var n=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("MDC: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: MDC: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                        var Master_ID=tempdata[1];
                        return;
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        var Slave_ID=this[n].Slave_ID;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var z;
                                var arr;
                                var h;
                                for(h=0;h<master_conn_table.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (tcp)
                                {
                                    arr = master_conn_table[h];
                                    if (arr.Master_ID == Master_ID)//есть мастер с ID запроса в таблице подключений tcp?
                                    {
                                        streams.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='MDC'+','+Master_ID+','+Slave_ID;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'MDC'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                                for(h=0;h<master_conn_table_usb.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (usb)
                                {
                                    arr = master_conn_table_usb[h];
                                    if (arr.Master_ID == Master_ID)//есть мастер с Master_ID в запросе в таблице подключений usb? - переслать данные мастеру
                                    {
                                        streams_usb.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                            if (arr.busNumber==stream.busNumber && arr.deviceAddress==stream.deviceAddress) {
                                                var endpoints = stream.interfaces[1].endpoints,// интерфейс 1 - интерфейс данных
                                                    inEndpoint = endpoints[1], // конечная точка 1 - входящие данные
                                                    outEndpoint = endpoints[0];// конечная точка 0 - исходящие данные
                                                var data='MDC'+','+Master_ID+','+Slave_ID;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'MDC'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                outEndpoint.transfer(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0)//слейв не найден - просто транслируем команду
                        {
                            intel.error('WARNING: MDC: Slave device with ID: ' + tempdata[2] +' not found in slave_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                            var z;
                            var arr;
                            var h;
                            for(h=0;h<master_conn_table.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (tcp)
                            {
                                arr = master_conn_table[h];
                                if (arr.Master_ID == Master_ID)//есть мастер с ID запроса в таблице подключений tcp?
                                {
                                    streams.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='MDC'+','+Master_ID+','+tempdata[2];
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'MDC'+','+Master_ID+','+tempdata[2]+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                            for(h=0;h<master_conn_table_usb.length;h++)//поиск мастера с такими же MASTER_ID в таблице подключений мастера (usb)
                            {
                                arr = master_conn_table_usb[h];
                                if (arr.Master_ID == Master_ID)//есть мастер с Master_ID в запросе в таблице подключений usb? - переслать данные мастеру
                                {
                                    streams_usb.forEach(function(stream){           //найти какому мастеру отправить (сравнить IP потока и IP из таблицы подключений мастеров)
                                        if (arr.busNumber==stream.busNumber && arr.deviceAddress==stream.deviceAddress) {
                                            var endpoints = stream.interfaces[1].endpoints,// интерфейс 1 - интерфейс данных
                                                inEndpoint = endpoints[1], // конечная точка 1 - входящие данные
                                                outEndpoint = endpoints[0];// конечная точка 0 - исходящие данные
                                            var data='MDC'+','+Master_ID+','+tempdata[2];
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'MDC'+','+Master_ID+','+tempdata[2]+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            outEndpoint.transfer(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("MDC: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}


/*********************************************************************
 * Function:         processCDM(tempdata, remoteAddress, remotePort)
 *
 * PreCondition:     master connected
 *
 * Input:
 * Return^           none
 *
 * Overview:
 *
 * Note:            Ответ мастера: Удален слейв (один слейв - одно сообщение)
 ********************************************************************/
function processCDM(tempdata, remoteAddress, remotePort)
{
    var dateNow = new Date();
    var year=dateNow.getFullYear();
    var month=dateNow.getMonth()+1;
    var date=dateNow.getDate();
    var time={};
    time.hours=dateNow.getHours();
    time.minutes=dateNow.getMinutes();
    time.seconds=dateNow.getSeconds();
    var strQuery=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?

    function check_table(callback) {
        connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            myNumber=rows;
            callback.call(myNumber);
        });
    }

    check_table(function () {
        var i;
        var k=0;
        var n=0;
        for (i=0;i<this.length;i++)
        {
            if (this[i].MASTER_ID_1==tempdata[1] || this[i].MASTER_ID_2==tempdata[1] || this[i].MASTER_ID_3==tempdata[1] || this[i].MASTER_ID_4==tempdata[1] || this[i].MASTER_ID_5==tempdata[1])//если совпадение найдено - поиск по таблице master_devices
            {
                k=1;
                var Customer_ID=this[i].Customer_ID;
                strQuery=("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
                check_table(function () {
                    var j=0;
                    for (n=0;n<this.length;n++)
                    {
                        if (this[n].Master_ID==tempdata[1])//Если да - идём дальше
                        {
                            j=1;
                            var Master_ID=this[n].Master_ID;
                        }
                    }
                    if (j==0)//совпадений не найдено - вывести ошибку
                    {
                        console.log("CDM: Master device with ID: " + tempdata[1] + " not found in master_devices'!");
                        intel.error('WARNING: CDM: Master device with ID: ' + tempdata[1] +' not found in master_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                        var Master_ID=tempdata[1];
                        return;
                    }
                    strQuery=("SELECT Slave_ID FROM slave_devices");//зарегистрирован ли такой SLAVE_ID в таблице slave_devices?
                    check_table(function () {
                        j=0;
                        var Slave_ID=this[n].Slave_ID;
                        for (n=0;n<this.length;n++)//SLAVE с таким ID уже существует в таблице slave_devices?
                        {
                            if (this[n].Slave_ID==tempdata[2])
                            {
                                j=1;
                                var z;
                                var arr;
                                var h;
                                for (z = 0; z < master_conn_table.length; z++)
                                {
                                    arr = master_conn_table[z];
                                    if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table[z] = arr;
                                        break;
                                    }
                                }
                                for (z = 0; z < master_conn_table_usb.length; z++)
                                {
                                    arr = master_conn_table_usb[z];
                                    if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                    {
                                        arr.Master_ID = Master_ID;
                                        master_conn_table_usb[z] = arr;
                                        break;
                                    }
                                }
                                for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                                {
                                    arr = client_conn_table[h];
                                    if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                    {
                                        streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                            if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                                var data='CDM'+','+Master_ID+','+Slave_ID;
                                                var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                                data='$'+'CDM'+','+Master_ID+','+Slave_ID+','+calc_checksum+'\r\n';//формировать строку на отправку
                                                stream.write(data, 'utf8');
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (j==0)//слейв не найден - просто транслируем команду
                        {
                            intel.error('WARNING: CDM: Slave device with ID: ' + tempdata[2] +' not found in slave_devices! '+ year +"-"+ month +"-"+ date +','+ time.hours +":"+ time.minutes +":"+ time.seconds+"\r\n");
                            var z;
                            var arr;
                            var h;
                            for (z = 0; z < master_conn_table.length; z++)
                            {
                                arr = master_conn_table[z];
                                if (arr.Port == remotePort && arr.IP == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table[z] = arr;
                                    break;
                                }
                            }
                            for (z = 0; z < master_conn_table_usb.length; z++)
                            {
                                arr = master_conn_table_usb[z];
                                if (arr.busNumber == remotePort && arr.deviceAddress == remoteAddress)//если есть такой адрес в таблице подключений  - записать в таблицу подключений клиекта все его MASTER_ID
                                {
                                    arr.Master_ID = Master_ID;
                                    master_conn_table_usb[z] = arr;
                                    break;
                                }
                            }
                            for(h=0;h<client_conn_table.length;h++)//поиск клиента с такими же MASTER_ID в таблице подключений клиента
                            {
                                arr = client_conn_table[h];
                                if (arr.Master_ID_1 == Master_ID || arr.Master_ID_2 == Master_ID || arr.Master_ID_3 == Master_ID || arr.Master_ID_4 == Master_ID || arr.Master_ID_5 == Master_ID)//если есть такой адрес есть в клиентской таблице подключений?
                                {
                                    streams.forEach(function(stream){           //найти кому отправить (сравнить IP потока и IP из таблице подключений клиентов)
                                        if (arr.IP==stream.remoteAddress && arr.Port==stream.remotePort) {
                                            var data='CDM'+','+Master_ID+','+tempdata[2];
                                            var calc_checksum=calc_xor(data).toString(16);//вычислять контрольную сумму
                                            data='$'+'CDM'+','+Master_ID+','+tempdata[2]+','+calc_checksum+'\r\n';//формировать строку на отправку
                                            stream.write(data, 'utf8');
                                        }
                                    });
                                }
                            }
                        }
                    });
                });
            }
        }
        if (k==0)
        {
            console.log("CDM: Master device with ID: " + tempdata[1] + " not registered in accaunts table!");
        }
    });
}







