var vow = require('vow');
var index = require("./index");


/**
 * Created by VenoM on 08.04.2017.
 */
function selectSensors(fields) {
    var strQuery=("SELECT Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time, Name, Lower_Value, Upper_Value, Delta FROM sensors WHERE Customer_ID = '"+ fields.Customer_ID +"'");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            resolve(rows);
        });
    })
}

function getSensorsLog(fields) {
    //seprately get all data
    if (fields.options.H){ // get hudmunity
        var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.customerId +"' AND Date BETWEEN '"+ fields.dateStart +"' AND '"+ fields.dateEnd +"' AND Time BETWEEN '"+ fields.timeStart +"' AND '"+ fields.timeEnd +"' ORDER BY Date AND Time ASC");
    }
    else{
        var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '0000000000000000' ");
    }
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            resolve(rows);
        });
    }).then (function (hudmLog) { // get ds18b20
        if (fields.options.ds18b20) {
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '" + fields.customerId + "' AND Date BETWEEN '" + fields.dateStart + "' AND '" + fields.dateEnd + "' AND Time BETWEEN '" + fields.timeStart + "' AND '" + fields.timeEnd + "' ORDER BY Date AND Time ASC");
        }
        else {
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '0000000000000000' ");
        }
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, tempLog) {
                if (err) throw err;
                resolve({hudmLog: hudmLog, tempLog: tempLog});
            });
        })
    }).then (function (data) {
        var hudmLog = data.hudmLog;
        var tempLog = data.tempLog;
        if (fields.options.R){
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'R' AND Customer_ID = '"+ fields.customerId +"' AND Date BETWEEN '"+ fields.dateStart +"' AND '"+ fields.dateEnd +"' AND Time BETWEEN '"+ fields.timeStart +"' AND '"+ fields.timeEnd +"' ORDER BY Date AND Time ASC");
        }
        else{
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'R' AND Customer_ID = '0000000000000000' ");
        }
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, relayLog) {
                if (err) throw err;
                resolve({hudmLog: hudmLog, tempLog: tempLog, relayLog: relayLog});
            });
        })
    }).then (function(data){
        var hudmLog = data.hudmLog;
        var tempLog = data.tempLog;
        var relayLog = data.relayLog;
        if (fields.options.Slave){
            var strQuery = ("SELECT * FROM slave_log WHERE Customer_ID = '"+ fields.customerId +"' AND Date BETWEEN '"+ fields.dateStart +"' AND '"+ fields.dateEnd +"' AND Time BETWEEN '"+ fields.timeStart +"' AND '"+ fields.timeEnd +"' ORDER BY Date AND Time ASC");
        }
        else{
            var strQuery = ("SELECT * FROM slave_log WHERE Customer_ID = '0000000000000000' ");
        }
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, slaveLog) {
                if (err) throw err;
                resolve({hudmLog: hudmLog, tempLog: tempLog, relayLog: relayLog, slaveLog: slaveLog});
            });
        })
    }).then (function(data) {
        var hudmLog = data.hudmLog;
        var tempLog = data.tempLog;
        var relayLog = data.relayLog;
        var slaveLog = data.slaveLog;
        if (fields.options.Slave) {
            var strQuery = ("SELECT * FROM master_log WHERE Customer_ID = '" + fields.customerId + "' AND Date BETWEEN '" + fields.dateStart + "' AND '" + fields.dateEnd + "' AND Time BETWEEN '" + fields.timeStart + "' AND '" + fields.timeEnd + "' ORDER BY Date AND Time ASC");
        }
        else {
            var strQuery = ("SELECT * FROM master_log WHERE Customer_ID = 'FFFFFF' AND Master_ID = 'FFFFFF' ");
        }
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, masterLog) {
                if (err) throw err;
                resolve({
                    hudmLog: hudmLog,
                    tempLog: tempLog,
                    relayLog: relayLog,
                    slaveLog: slaveLog,
                    masterLog: masterLog
                });
            });
        })
    })
}

function selectLastSensLogTemp(fields) {
    switch (fields.LastID)
    {
        case "TempReq1Min"://key word for a new Graph preload; 1Minute
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '"+ fields.Customer_ID +"' AND Time >= DATE_SUB(NOW(), INTERVAL 1 MINUTE) AND Date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Date AND Time ASC");
            break
        case "TempBar1Hour"://key word for a new Graph preload; 1Hour
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '"+ fields.Customer_ID +"' AND Time >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND Date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Date AND Time ASC");
            break
        case "TempBar6Hour"://6 Hours
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '"+ fields.Customer_ID +"' AND Time >= DATE_SUB(NOW(), INTERVAL 6 HOUR) AND Date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Date AND Time ASC");
            break
        case "TempBar1Day"://1 Day
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '"+ fields.Customer_ID +"' AND date >= DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) ORDER BY `date` ASC");
            break
        case "TempBar1Week":// 1 Week
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '"+ fields.Customer_ID +"' AND date >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) ORDER BY `date` ASC ");
            break
        case "TempBar3Month":// 3 Month
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '"+ fields.Customer_ID +"' AND date >= DATE_SUB(CURRENT_DATE, INTERVAL 3 MONTH) ORDER BY `date` ASC");
            break
        default: break
    }
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            resolve(rows);
        });
    }).then (function (rows){
        if (rows.length >= 800000){
            var data = rows;
            return new vow.Promise(function (resolve, reject, notify) {
                var strQuery = ("DELETE  FROM sensors_log WHERE Type = 'ds18b20' AND Customer_ID = '"+ fields.Customer_ID +"' AND date < DATE_SUB(NOW(), INTERVAL 3 MONTH)");//удалить записи старше месяца
                index.connection.query(strQuery, function (err, rows) {
                    if (err) throw err;
                    resolve(data);
                    data = 0;
                })
            })
        }
        else{
            return rows;
        }
    })
}

function selectLastSensLogHudm(fields) {
    switch (fields.LastID)
    {
        case "HudmReq1Min"://key word for a new Graph preload; 1Minute
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' AND Time >= DATE_SUB(NOW(), INTERVAL 1 MINUTE) AND Date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Date AND Time ASC");
            break
        case "HudmBar1Hour"://key word for a new Graph preload; 1Hour
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' AND Time >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND Date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Date AND Time ASC");
            break
        case "HudmBar6Hour"://6 Hours
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' AND Time >= DATE_SUB(NOW(), INTERVAL 6 HOUR) AND Date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Date AND Time ASC");
            break
        case "HudmBar1Day"://1 Day
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' AND date >= DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) ORDER BY `date` ASC");
            break
        case "HudmBar1Week":// 1 Week
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' AND date >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) ORDER BY `date` ASC ");
            break
        case "HudmBar3Month":// 3 Month
            var strQuery = ("SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' AND date >= DATE_SUB(CURRENT_DATE, INTERVAL 3 MONTH) ORDER BY `date` ASC");
            break
        default: break
    }
    // var strQuery=("SELECT * FROM (SELECT * FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' ORDER BY id DESC LIMIT "+ fields.LastID +") t ORDER BY id");//select by ID count;
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery, function (err, rows) {
            if (err) throw err;
            resolve(rows);
        });
    }).then (function (rows){
        if (rows.length >= 800000){
            var data = rows;
            return new vow.Promise(function (resolve, reject, notify) {
                var strQuery = ("DELETE  FROM sensors_log WHERE Type = 'H' AND Customer_ID = '"+ fields.Customer_ID +"' AND date < DATE_SUB(NOW(), INTERVAL 3 MONTH)");//удалить записи старше месяца
                index.connection.query(strQuery, function (err, rows) {
                    if (err) throw err;
                    resolve(data);
                    data = 0;
                })
            })
        }
        else{
            return rows;
        }

    })
}

function selectMasters() {
    var strQuery1=("SELECT Name, Customer_ID, MASTER_ID_1, MASTER_ID_2, MASTER_ID_3, MASTER_ID_4, MASTER_ID_5 FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery1, function (err, rows) {
            if (err) throw err;
            resolve(rows);
        });
        // now you can resolve, reject, notify the promise
    }).then(function(rows) {
        strQuery2 = ("SELECT Master_ID FROM master_devices");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery2, function (err, rows1) {
                if (err) throw err;
                resolve(rows1);
            })
        })
    })
}

function setSensorName(fields) {
    var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
    return new vow.Promise(function(resolve, reject, notify) {
            index.connection.query(strQuery1, function (err, sensors) {
                if (err) throw err;
                resolve(sensors);
            });
        // now you can resolve, reject, notify the promise
    }).then(function(sensors) {
        var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    }).then(function (data){
        // index.connection.query("UPDATE `sensors_setting` SET `Name` =('444') WHERE  `sensors_setting`.`Type` ='H';");//обновить Name для этого датчика (если существует)
        // index.connection.query("UPDATE `sensors_setting` SET `Name` =('aaa') WHERE  `sensors_setting`.`Type` ='ds18b20';");//обновить Name для этого датчика (если существует)
        var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
        var sensors = data.sensors;
        var counter = 0;
        var matchedFlag = false;
        var sensors_settings = data.sensors_setting;
        if (fields.Name != undefined && sensors != undefined)
        {
            if (sensors_settings.length != 0)//insert or update field
            {
                sensors_settings.forEach(function (sensor_set) {
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name != sensor_set.Name)//all parameters the same exept "Name"-->update fields
                    {
                        index.connection.query("UPDATE LOW_PRIORITY `sensors` SET `Name` =('" + fields.Name + "') WHERE  `sensors`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors`.`Address` ='" + sensor_set.Address + "' AND `sensors`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Name` =('" + fields.Name + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить время
                        matchedFlag = true;
                    }
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name == sensor_set.Name)//all parameters the same exept "Name"-->update fields
                    {
                        matchedFlag = true;
                    }
                    counter++;
                });
                if (matchedFlag == false)
                {
                    index.connection.query("UPDATE `sensors` SET `Name` =('" + fields.Name + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить дату
                    index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "');");
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить время
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Name` =('" + fields.Name + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить name
                    matchedFlag = false;
                }
            }
            else{
                index.connection.query("UPDATE `sensors` SET `Name` =('" + fields.Name + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить дату
                index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "');");
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить время
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Name` =('" + fields.Name + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить name
            }
        }
        return new vow.Promise(function (resolve, reject, notify) {
            var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    })
}

function setLowerValue(fields) {
    var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery1, function (err, sensors) {
            if (err) throw err;
            resolve(sensors);
        });
    }).then(function(sensors) {
        var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    }).then(function (data){
        var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
        var sensors = data.sensors;
        var counter = 0;
        var matchedFlag = false;
        var sensors_settings = data.sensors_setting;
        if (fields.Name != undefined && sensors != undefined)
        {
            if (sensors_settings.length != 0)//insert or update field
            {
                sensors_settings.forEach(function (sensor_set) {
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name == sensor_set.Name)//all parameters the same exept "Name"-->update fields
                    {
                        index.connection.query("UPDATE LOW_PRIORITY `sensors` SET `Lower_Value` =('" + fields.Lower_Value + "') WHERE  `sensors`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors`.`Address` ='" + sensor_set.Address + "' AND `sensors`.`Type` ='" + sensor_set.Type + "';");//обновить LowerValue
                        index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Lower_Value` =('" + fields.Lower_Value + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить время
                        matchedFlag = true;
                    }
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name == sensor_set.Name && fields.LowerValue == sensor_set.LowerValue)//all parameters the same
                    {
                        matchedFlag = true;
                    }
                    counter++;
                });
                if (matchedFlag == false)
                {
                    index.connection.query("UPDATE `sensors` SET `Lower_Value` =('" + fields.Lower_Value + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить LowerValue
                    index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name, Lower_Value) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "', '" + fields.Lower_Value + "');");
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Time
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Date
                    matchedFlag = false;
                }
            }
            else{
                index.connection.query("UPDATE `sensors` SET `Lower_Value` =('" + fields.Name + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить дату
                index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name, Lower_Value) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "', '" + fields.Lower_Value + "');");
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Time
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Date
            }
        }
        return new vow.Promise(function (resolve, reject, notify) {
            var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    })
}

function setUpperValue(fields) {
    var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery1, function (err, sensors) {
            if (err) throw err;
            resolve(sensors);
        });
    }).then(function(sensors) {
        var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    }).then(function (data){
        var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
        var sensors = data.sensors;
        var counter = 0;
        var matchedFlag = false;
        var sensors_settings = data.sensors_setting;
        if (fields.Name != undefined && sensors != undefined)
        {
            if (sensors_settings.length != 0)//insert or update field
            {
                sensors_settings.forEach(function (sensor_set) {
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name == sensor_set.Name)//all parameters the same exept "Name"-->update fields
                    {
                        index.connection.query("UPDATE  `sensors` SET `Upper_Value` =('" + fields.Upper_Value + "') WHERE  `sensors`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors`.`Address` ='" + sensor_set.Address + "' AND `sensors`.`Type` ='" + sensor_set.Type + "';");//обновить LowerValue
                        index.connection.query("UPDATE  `sensors_setting` SET `Upper_Value` =('" + fields.Upper_Value + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE  `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Date
                        index.connection.query("UPDATE  `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Time
                        matchedFlag = true;
                    }
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name == sensor_set.Name && fields.Upper_Value == sensor_set.Upper_Value)//all parameters the same
                    {
                        matchedFlag = true;
                    }
                    counter++;
                });
                if (matchedFlag == false)
                {
                    index.connection.query("UPDATE `sensors` SET `Upper_Value` =('" + fields.Upper_Value + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить LowerValue
                    index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name, Upper_Value) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "', '" + fields.Upper_Value + "');");
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Time
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Date
                    matchedFlag = false;
                }
            }
            else{
                index.connection.query("UPDATE `sensors` SET `Upper_Value` =('" + fields.Name + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить дату
                index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name, Upper_Value) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "', '" + fields.Upper_Value + "');");
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Time
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Date
            }
        }
        return new vow.Promise(function (resolve, reject, notify) {
            var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    })
}

function setDelta(fields) {
    var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery1, function (err, sensors) {
            if (err) throw err;
            resolve(sensors);
        });
    }).then(function(sensors) {
        var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
        return new vow.Promise(function (resolve, reject, notify) {
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    }).then(function (data){
        var strQuery1=("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
        var sensors = data.sensors;
        var counter = 0;
        var matchedFlag = false;
        var sensors_settings = data.sensors_setting;
        if (fields.Name != undefined && sensors != undefined)
        {
            if (sensors_settings.length != 0)//insert or update field
            {
                sensors_settings.forEach(function (sensor_set) {
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name == sensor_set.Name)//all parameters the same -->update fields
                    {
                        index.connection.query("UPDATE  `sensors` SET `Delta` =('" + fields.Delta + "') WHERE  `sensors`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors`.`Address` ='" + sensor_set.Address + "' AND `sensors`.`Type` ='" + sensor_set.Type + "';");//обновить LowerValue
                        index.connection.query("UPDATE  `sensors_setting` SET `Delta` =('" + fields.Delta + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE  `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить дату
                        index.connection.query("UPDATE  `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить время
                        matchedFlag = true;
                    }
                    if(fields.Customer_ID == sensor_set.Customer_ID && fields.Master_ID == sensor_set.Master_ID && fields.Slave_ID == sensor_set.Slave_ID && fields.Type == sensor_set.Type && fields.Address == sensor_set.Address && fields.Name == sensor_set.Name && fields.Delta == sensor_set.Delta)//all parameters the same
                    {
                        matchedFlag = true;
                    }
                    counter++;
                });
                if (matchedFlag == false)
                {
                    index.connection.query("UPDATE `sensors` SET `Delta` =('" + fields.Delta + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить Delta
                    index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name, Delta) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "', '" + fields.Delta + "');");
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить время
                    index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить Time
                    matchedFlag = false;
                }
            }
            else{
                index.connection.query("UPDATE `sensors` SET `Delta` =('" + fields.Delta + "') WHERE  `sensors`.`Slave_ID` ='" + fields.Slave_ID + "' AND `sensors`.`Address` ='" + fields.Address + "' AND `sensors`.`Type` ='" + fields.Type + "';");//обновить дату
                index.connection.query("INSERT INTO `sensors_setting`(Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Name, Delta) VALUES ('" + fields.Customer_ID + "','" + fields.Master_ID + "','" + fields.Slave_ID + "','" + fields.Type + "', '" + fields.Address + "', '" + fields.Value + "', '" + fields.Name + "', '" + fields.Delta + "');");
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Time` =('" + fields.Time + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить время
                index.connection.query("UPDATE LOW_PRIORITY `sensors_setting` SET `Date` =('" + fields.Date + "') WHERE  `sensors_setting`.`Slave_ID` ='" + sensor_set.Slave_ID + "' AND `sensors_setting`.`Address` ='" + sensor_set.Address + "' AND `sensors_setting`.`Type` ='" + sensor_set.Type + "';");//обновить name
            }
        }
        return new vow.Promise(function (resolve, reject, notify) {
            var strQuery = ("SELECT Name, Customer_ID, Master_ID, Slave_ID, Type, Address, Value, Date, Time FROM sensors_setting");//зарегистрирован ли такой MASTER_ID в таблице master_devices?
            index.connection.query(strQuery, function (err, sensors_setting) {
                if (err) throw err;
                resolve({sensors_setting: sensors_setting, sensors: sensors, fields: fields});
            })
        })
    })
}

function selectAcc() {
    var strQuery1=("SELECT Customer_ID, Login, Password  FROM accaunts");//зарегистрирован ли такой MASTER_ID в таблице accaunts?
    return new vow.Promise(function(resolve, reject, notify) {
        index.connection.query(strQuery1, function (err, rows) {
            if (err) throw err;
            resolve(rows);
        });
    })
}

exports.selectLastSensLogHudm = selectLastSensLogHudm;
exports.selectLastSensLogTemp = selectLastSensLogTemp;
exports.selectSensors = selectSensors;
exports.selectMasters = selectMasters;
exports.selectAcc = selectAcc;

exports.setSensorName = setSensorName;
exports.setLowerValue = setLowerValue;
exports.setUpperValue = setUpperValue;
exports.setDelta = setDelta;

exports.getSensorsLog = getSensorsLog;