/**
 * Created by VenoM on 22.02.2017.
 */
function route(pathname, handle, response, fields) {
    console.log("About to route a request for " + pathname);
    if (typeof handle[pathname] === 'function')
    {
        handle[pathname](response, fields);
    }

    else
    {
        console.log("No request handler found for " + pathname);
    }
}

exports.route = route;